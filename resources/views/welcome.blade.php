<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Checkyuk - Your Daily Legitchecker!</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">
</head>
<body>
    @include('includes.header')

    <div class="site-blocks-cover overlay" style="background-image: url({{$page_config->welcome_background_image}});" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div style="padding-top:5%;">
            @include('includes.flash-message')
        </div>
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1">{{$page_config->welcome_caption1}} </h3>
                <h3 data-aos="fade-up" class="mb-5">{{$page_config->welcome_caption2}} </h3>

                <p data-aos="fade-up" data-aos-delay="100"><a href="{{ url('authentication-service')}}" class="btn btn-pill" style="background:#00C89E;color:white;font-weight:bold;">CLICK HERE TO CHECK</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    @include('includes.whychooseus')

    <!-- Logo Carousel Begin -->
    <div class="logo-carousel">
        <div class="container">
            <div class="lc-slider owl-carousel">
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/balenciaga-logo-0.png" alt="">
                    </div>
                </a>
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/gucci-logo-gold.png" alt="">
                    </div>
                </a>
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/louis-vuitton-logo-0.png" alt="">
                    </div>
                </a>
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/goyard-logo-0.png" alt="">
                    </div>
                </a>
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/chanel-logo-0.png" alt="">
                    </div>
                </a>
                <a href="#" class="lc-item">
                    <div class="lc-item-inner">
                        <img src="{{ asset('uploads/logo-car')}}ousel/nike-logo-png.png" alt="">
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- Logo Carousel End -->

    <!-- Contact Section Begin -->
    <!-- <section class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-info">
                        <div class="ci-item">
                            <div class="ci-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Address</h5>
                                <p>160 Pennsylvania Ave NW, Washington, Castle, PA 16101-5161</p>
                            </div>
                        </div>
                        <div class="ci-item">
                            <div class="ci-icon">
                                <i class="fa fa-mobile"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Phone</h5>
                                <ul>
                                    <li>125-711-811</li>
                                    <li>125-668-886</li>
                                </ul>
                            </div>
                        </div>
                        <div class="ci-item">
                            <div class="ci-icon">
                                <i class="fa fa-headphones"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Support</h5>
                                <p>Support.aler@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cs-map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d735515.5813275519!2d-80.41163541934742!3d43.93644386501528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882a55bbf3de23d7%3A0x3ada5af229b47375!2sMono%2C%20ON%2C%20Canada!5e0!3m2!1sen!2sbd!4v1583262687289!5m2!1sen!2sbd"
                height="450" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section> -->
    <!-- Contact Section End -->

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.richtext.min.js"></script>
    <script src="js/image-uploader.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>