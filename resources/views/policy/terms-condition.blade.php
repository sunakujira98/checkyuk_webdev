<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Aler, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Term And Condition</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style2.css')}}">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Wrapper Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="canvas-close">
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class="logo">
            <a href="{{ url('/')}}">
                <img src="{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
            </a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="om-widget">
            <ul>
                @guest
                    <li>
                        <a href="{{ route('login') }}" style="margin-right:-42px;"><i class="fa fa-sign-in"></i>{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li>
                            <a href="{{ route('register') }}" style="margin-right:-49px;"><i class="fa fa-user"></i>{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                <li>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i>{{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endguest
            </ul>
        </div>
        <div class="om-social">
            <a href="{{$page_config->instagram_account}}" target="_blank"><i class="fa fa-instagram"></i></a>
            <a href="{{$page_config->whatsapp_api}}" target="_blank"><i class="fa fa-mobile"></i></a>
        </div>
    </div>
    <!-- Offcanvas Menu Wrapper End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="hs-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo-responsive">
                            <a href="{{url ('/')}}">
                                <img src="{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" style="max-width:10px:" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="ht-widget">
                            <ul>
                                @if(Session::has('cart'))
                                    <li>
                                        <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;margin-right:-20px;"><i class="fa fa-shopping-cart"></i></a>
                                        <span class="badge badge-warning navbar-badge">{{Session::get('cart')->totalQty}}</span>
                                    </li>
                                @else
                                    <li>
                                        <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;margin-right:-20px;"><i class="fa fa-shopping-cart"></i></a>
                                        <span class="badge badge-warning navbar-badge">0</span>
                                    </li>            
                                @endif
                                @guest
                                    <li style="margin-right:0px;">
                                        <a href="{{ route('login') }}"><i class="fa fa-sign-in"></i>{{ __('Login') }}</a>
                                    </li>
                                    @if (Route::has('register'))
                                        <li style="margin-right:0px;">
                                            <a href="{{ route('register') }}"><i class="fa fa-user"></i>{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                    <li style="margin-right:0px;">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="canvas-open" style="margin-right:50px;">
                    @if(Session::has('cart'))
                        <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;"><i class="fa fa-shopping-cart"></i></a>
                        <span class="badge badge-warning navbar-badge" style="top:-10px;right:25px;position:absolute;">{{Session::get('cart')->totalQty}}</span>
                    @else
                        <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;"><i class="fa fa-shopping-cart"></i></a>
                        <span class="badge badge-warning navbar-badge" style="top:-10px;right:25px;position:absolute;">0</span>
                    @endif
                </div>

                <div class="canvas-open">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
        </div>
        
        <div class="hs-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <nav class="nav-menu">
                            <ul>
                                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/')}}">Home</a></li>
                                <li class="{{ Request::is('authentication-service') || request()->is('search-brand') ? 'active' : '' }}"><a href="{{ url('authentication-service')}}">Jasa Otentikasi</a>
                                @if(isset(Auth::user()->email))
                                <li class="{{ Request::is('lc-history') || request()->is('lc-history-view*') ? 'active' : '' }}"><a href="{{ url('lc-history')}}">My LC History</a>
                                @endif
                                    <!-- <ul class="dropdown">
                                        <li><a href="./property.html">Property Grid</a></li>
                                        <li><a href="./profile.html">Property List</a></li>
                                        <li><a href="./property-details.html">Property Detail</a></li>
                                        <li><a href="./property-comparison.html">Property Comperison</a></li>
                                        <li><a href="./property-submit.html">Property Submit</a></li>
                                    </ul> -->
                                </li>
                                <li class="{{ Request::is('legit-check-history') || request()->is('legit-check-history-detail*') ? 'active' : '' }}"><a href="{{ url('legit-check-history')}}">History</a>
                                <li class="{{ Request::is('about-us-page') ? 'active' : '' }}"><a href="{{ url('about-us-page')}}">About Us</a>
                                <li class="{{ Request::is('article-page') ? 'active' : '' }}"><a href="{{ url('article-page')}}">Artikel</a>
                                <!-- <li><a href="./agents.html">Agents</a></li>
                                <li><a href="./about.html">About</a></li>
                                <li><a href="./blog.html">Blog</a></li>
                                <li><a href="./contact.html">Contact</a></li> -->
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="hn-social">
                            <a href="{{$page_config->instagram_account}}" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="{{$page_config->whatsapp_api}}" target="_blank"><i class="fa fa-whatsapp"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <div class="site-blocks-cover overlay" style="background-image: url({{$page_config->termscondition_background_image}});" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div style="padding-top:5%;">
            @include('includes.flash-message')
        </div>
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1">{{$page_config->termscondition_caption1}}</h3>
                <h4 data-aos="fade-up" class="mb-1">{{$page_config->termscondition_caption2}} </h4>
                <h4 data-aos="fade-up" class="mb-5">{{$page_config->termscondition_caption3}} </h4>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- About Section Begin -->
    <section class="about-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="about-text">
                        <div class="at-title">
                            <h3>{{$page_config->termscondition_title}}</h3>
                        </div>
                        <div class="at-feature">
                            <div class="af-item">
                                <div class="af-icon">
                                    <img src="uploads/chooseus/chooseus-icon-1.png" alt="">
                                </div>
                                <div class="af-item">
                                <div class="af-text">
                                    <table>
                                    </table>
                                    @if($tc_policy!=NULL)
                                    <p>{!!$tc_policy->caption!!}</p>
                                    @endif
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.richtext.min.js"></script>
    <script src="js/image-uploader.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>