<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Checkyuk, LegitCheck, LegitChecker, ShoeAuthentication">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Photo Guides</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    
    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style2.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{ asset('css/fileinput/fileinput.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/fileinput/theme.min.css')}}">
</head>

<body>
    @include('includes.header')

    <div class="site-blocks-cover overlay" style="background-image: url({{$page_config->photos_guide_background_image}});" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div style="padding-top:5%;">
            @include('includes.flash-message')
        </div>
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1">{{$page_config->photos_guide_caption1}} </h3>
                <h4 data-aos="fade-up" class="mb-1">{{$page_config->photos_guide_caption2}} </h4>
                <h4 data-aos="fade-up" class="mb-5">{{$page_config->photos_guide_caption3}} </h4>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- Agent Section Begin -->
    <section class="agent-section spad">
        <div class="container">
            <div class="row">
                @foreach($photos_guide as $pg)
                <div class="col-lg-4 col-md-6">
                    <div class="as-item">
                        <div class="as-text">
                            <div class="at-title">
                                <h6>{{$pg->title}}</h6>
                            </div>

                            <a href="{{asset ($pg->image_url)}}" data-lightbox="image-1"><img src="{{$pg->image_url}}" alt=""></a>
                            <div style="text-align:left">
                              {!!$pg->caption!!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        {{$photos_guide->links()}}
        </div>
    </section>
    <!-- Agent Section End -->
    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>