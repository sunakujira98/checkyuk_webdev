<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Aler, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Artikel Detail</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style2.css')}}">
</head>

<body>
    @include('includes.header')
    
    <!-- Blog Hero Section Begin -->
    <section class="blog-hero-section set-bg" data-setbg="{{ asset($article_details->image)}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bh-text">
                        <h4>{{$article_details->title}}</h4>
                        <ul>
                            <li>by <span>Checkyuk</span></li>
                            <li>{{$article_details->created_at->format('d-M-y')}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Hero Section End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 m-auto p-0">
                    <div class="blog-details-content">
                        <div class="bc-top">
                            {!!$article_details->content!!}
                        </div>
                        <div class="bc-tags">
                            @foreach($article_details->tags as $tag)
                            <a href="#">{{$tag->name}}</a>
                            @endforeach
                        </div>
                        <!-- <div class="bc-related-post">
                            <a href="#" class="previous-post"><i class="fa fa-angle-left"></i> Previous posts</a>
                            <a href="#" class="next-post">Next posts <i class="fa fa-angle-right"></i></a>
                        </div> -->
                        <!-- <div class="bc-widget">
                            <h4>Related posts</h4>
                            <div class="related-post">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="related-item">
                                            <div class="ri-pic">
                                                <img src="img/blog/related-post-1.jpg" alt="">
                                            </div>
                                            <div class="ri-text">
                                                <h6>what3words: The app changin real estate and construc...</h6>
                                                <span>Seb 24, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="related-item">
                                            <div class="ri-pic">
                                                <img src="img/blog/related-post-2.jpg" alt="">
                                            </div>
                                            <div class="ri-text">
                                                <h6>what3words: The app changin real estate and construc...</h6>
                                                <span>Seb 24, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="related-item">
                                            <div class="ri-pic">
                                                <img src="img/blog/related-post-3.jpg" alt="">
                                            </div>
                                            <div class="ri-text">
                                                <h6>what3words: The app changin real estate and construc...</h6>
                                                <span>Seb 24, 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="bc-widget">
                            <h4>3 Comment</h4>
                            <div class="comment-option">
                                <div class="co-item">
                                    <div class="ci-pic">
                                        <img src="img/blog/comment/comment-1.jpg" alt="">
                                    </div>
                                    <div class="ci-text">
                                        <h5>Brandon Kelley</h5>
                                        <p>Duis voluptatum. Id vis consequat consetetur dissentiet, ceteros commune
                                            perpetua mei et. Simul viderer facilisis egimus tractatos splendi.</p>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Seb 17, 2019</li>
                                            <li><i class="fa fa-heart-o"></i> 12</li>
                                            <li><i class="fa fa-share-square-o"></i> 1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="co-item reply-item">
                                    <div class="ci-pic">
                                        <img src="img/blog/comment/comment-2.jpg" alt="">
                                    </div>
                                    <div class="ci-text">
                                        <h5>Brandon Kelley</h5>
                                        <p>Duis voluptatum. Id vis consequat consetetur dissentiet, ceteros commune
                                            perpetua mei et. Simul viderer facilisis egimus tractatos splendi.</p>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Seb 17, 2019</li>
                                            <li><i class="fa fa-heart-o"></i> 12</li>
                                            <li><i class="fa fa-share-square-o"></i> 1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="co-item">
                                    <div class="ci-pic">
                                        <img src="img/blog/comment/comment-3.jpg" alt="">
                                    </div>
                                    <div class="ci-text">
                                        <h5>Brandon Kelley</h5>
                                        <p>Duis voluptatum. Id vis consequat consetetur dissentiet, ceteros commune
                                            perpetua mei et. Simul viderer facilisis egimus tractatos splendi.</p>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Seb 17, 2019</li>
                                            <li><i class="fa fa-heart-o"></i> 12</li>
                                            <li><i class="fa fa-share-square-o"></i> 1</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bc-widget">
                            <h4>Leave a comment</h4>
                            <form action="#" class="leave-comment-form">
                                <div class="group-input">
                                    <input type="text" placeholder="Name">
                                    <input type="text" placeholder="Email">
                                    <input type="text" placeholder="Website">
                                </div>
                                <textarea placeholder="Comment"></textarea>
                                <button type="submit" class="site-btn">Submit</button>
                            </form>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>