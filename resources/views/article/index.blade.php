<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Checkyuk, LegitCheck, LegitChecker, ShoeAuthentication">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Artikel</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style2.css')}}">
</head>

<body>
    @include('includes.header')

    <!-- Blog Section Begin -->
    <section class="blog-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-item-list">
                        @foreach($article as $articles)
                        <div class="blog-item large-blog">
                            <div class="bi-pic">
                                <img src="{{$articles->image}}" alt="">
                            </div>
                            <div class="bi-text">
                                <h4><a href="{{ route('blog.details', ['slug' => $articles->slug ])}}">{{$articles->title}}</a></h4>
                                <ul>
                                    <li>by <span>Checkyuk</span></li>
                                    <li>{{$articles->created_at->format('d-M-y')}}</li>
                                    <!-- <li>12 Comment</li> -->
                                </ul>
                                <p>{{$articles->title}}...</p>
                                <a href="{{ route('blog.details', ['slug' => $articles->slug ])}}" class="read-more">Baca artikel <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        @endforeach
                        <!-- <div class="blog-item">
                            <div class="bi-pic">
                                <img src="uploads/blog/blog-2.jpg" alt="">
                            </div>
                            <div class="bi-text">
                                <h5><a href="./blog-details.html">what3words: The app changing real estate and construction forever</a></h5>
                                <ul>
                                    <li>by <span>Jonathan Doe</span></li>
                                    <li>Seb 24, 2019</li>
                                    <li>12 Comment</li>
                                </ul>
                                <p>Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown...</p>
                                <a href="#" class="read-more">Read more <span class="arrow_right"></span></a>
                            </div>
                        </div> -->
                    </div>
                    <!-- <div class="blog-pagination property-pagination ">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#" class="icon"><span class="arrow_right"></span></a>
                    </div> -->
                </div>

                <!-- <div class="col-lg-4">
                    <div class="blog-sidebar">
                        <div class="follow-us">
                            <div class="section-title sidebar-title-b">
                                <h6>Follow us</h6>
                            </div>
                            <div class="fu-links">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="feature-post">
                            <div class="section-title sidebar-title-b">
                                <h6>Feature posts</h6>
                            </div>
                            <div class="recent-post">
                                <div class="rp-item">
                                    <div class="rp-pic">
                                        <img src="uploads/blog/rp-1.jpg" alt="">
                                    </div>
                                    <div class="rp-text">
                                        <h6><a href="#">Vancouver real estate advisurges conference goers...</a></h6>
                                        <span>Seb 24, 2019</span>
                                    </div>
                                </div>
                                <div class="rp-item">
                                    <div class="rp-pic">
                                        <img src="uploads/blog/rp-2.jpg" alt="">
                                    </div>
                                    <div class="rp-text">
                                        <h6><a href="#">Vancouver real estate advisurges conference goers...</a></h6>
                                        <span>Seb 24, 2019</span>
                                    </div>
                                </div>
                                <div class="rp-item">
                                    <div class="rp-pic">
                                        <img src="uploads/blog/rp-3.jpg" alt="">
                                    </div>
                                    <div class="rp-text">
                                        <h6><a href="#">Vancouver real estate advisurges conference goers...</a></h6>
                                        <span>Seb 24, 2019</span>
                                    </div>
                                </div>
                                <div class="rp-item">
                                    <div class="rp-pic">
                                        <img src="uploads/blog/rp-4.jpg" alt="">
                                    </div>
                                    <div class="rp-text">
                                        <h6><a href="#">Vancouver real estate advisurges conference goers...</a></h6>
                                        <span>Seb 24, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
    </section>
    <!-- Blog Section End -->

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.richtext.min.js"></script>
    <script src="js/image-uploader.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>