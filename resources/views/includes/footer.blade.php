<!-- Footer Section Begin -->
<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="fs-about">
                    <div class="fs-logo">
                        <a href="#">
                            <img src="{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-top:-100px;margin-bottom:-75px;">
                        </a>
                    </div>
                    <p>{{$page_config->footer_caption}}</p>
                    <div class="fs-social">
                        <a href="{{$page_config->instagram_account}}" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="{{$page_config->whatsapp_api}}" target="_blank"><i class="fa fa-mobile"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="fs-widget">
                    <h5>Help</h5>
                    <ul style="padding:0;margin:0;">
                        <li><a href="{{url('about-us-page')}}">About Us</a></li>
                        <li><a href="{{url('terms-condition-page')}}">Terms and condition</a></li>
                        <li><a href="{{url('refund-policy-page')}}">Refund Policy</a></li>
                        <li><a href="{{url('photos-guide-page')}}">Photos Guide</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="fs-widget">
                    <h5>Links</h5>
                    <ul style="padding:0;margin:0;">
                        <li><a href="{{url('authentication-service')}}">Jasa Otentikasi</a></li>
                        <li><a href="{{url('lc-history')}}">History</a></li>
                        <li><a href="{{url('article-page')}}">Artikel</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="col-lg-4 col-md-6">
                <div class="fs-widget">
                    <h5>Newsletter</h5>
                    <p>Deserunt mollit anim id est laborum.</p>
                    <form action="#" class="subscribe-form">
                        <input type="text" placeholder="Email">
                        <button type="submit" class="site-btn">Subscribe</button>
                    </form>
                </div>
            </div> -->
        </div>
        <div class="copyright-text">
            <p>
                Copyright &copy;<script>
                    document.write(new Date().getFullYear());
                </script> All rights reserved | This website is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="mailto:steventheodore98@gmail.com?subject=I get your contact from checkyuk.com">Steven Theodore</a>
            </p>
        </div>
    </div>
</footer>
<!-- Footer Section End -->