<!-- Chooseus Section Begin -->
<section class="chooseus-section spad set-bg" data-setbg="{{$page_config->whychooseus_background_image}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="chooseus-text">
                        <div class="section-title">
                            <h4>{{$page_config->whychooseus_title}}</h4>
                        </div>
                        <p>{{$page_config->whychooseus_caption}}</p>
                    </div>
                    <div class="chooseus-features">
                        @foreach($why_choose_us as $choose)
                        <div class="cf-item">
                            <div class="cf-pic">
                                <img src="{{$choose->image_url}}" alt="" height="50px" width="50px">
                            </div>
                            <div class="cf-text">
                                <h5>{{$choose->judul}}</h5>
                                <p>{{$choose->caption}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Chooseus Section End -->