<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Checkyuk Template">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
</head>

<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Wrapper Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>
    <div class="logo">
        <a href="{{ url('/')}}">
            <img src="{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
        </a>
    </div>
    <div id="mobile-menu-wrap"></div>
    <div class="om-widget">
        <ul>
            @guest
                <li>
                    <a href="{{ route('login') }}" style="margin-right:-42px;"><i class="fa fa-sign-in"></i>{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li>
                        <a href="{{ route('register') }}" style="margin-right:-49px;"><i class="fa fa-user"></i>{{ __('Register') }}</a>
                    </li>
                @endif
            @else
            <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i>{{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            @endguest
        </ul>
    </div>
    <div class="om-social">
        <a href="{{$page_config->instagram_account}}" target="_blank"><i class="fa fa-instagram"></i></a>
        <a href="{{$page_config->whatsapp_api}}" target="_blank"><i class="fa fa-mobile"></i></a>
    </div>
</div>
<!-- Offcanvas Menu Wrapper End -->

<!-- Header Section Begin -->
<header class="header-section">
    <div class="hs-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo-responsive">
                        <a href="{{url ('/')}}">
                            <img src="{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" style="max-width:10px:" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="ht-widget">
                        <ul>
                            @if(Session::has('cart'))
                                <li>
                                    <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;margin-right:-20px;"><i class="fa fa-shopping-cart"></i></a>
                                    <span class="badge badge-warning navbar-badge">{{Session::get('cart')->totalQty}}</span>
                                </li>
                            @else
                                <li>
                                    <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;margin-right:-20px;"><i class="fa fa-shopping-cart"></i></a>
                                    <span class="badge badge-warning navbar-badge">0</span>
                                </li>            
                            @endif
                            @guest
                                <li style="margin-right:0px;">
                                    <a href="{{ route('login') }}"><i class="fa fa-sign-in"></i>{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li style="margin-right:0px;">
                                        <a href="{{ route('register') }}"><i class="fa fa-user"></i>{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li style="margin-right:0px;">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="canvas-open" style="margin-right:50px;">
                @if(Session::has('cart'))
                    <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;"><i class="fa fa-shopping-cart"></i></a>
                    <span class="badge badge-warning navbar-badge" style="top:-10px;right:25px;position:absolute;">{{Session::get('cart')->totalQty}}</span>
                @else
                    <a data-toggle="modal" data-target="#modal-default" style="cursor: pointer;"><i class="fa fa-shopping-cart"></i></a>
                    <span class="badge badge-warning navbar-badge" style="top:-10px;right:25px;position:absolute;">0</span>
                @endif
            </div>

            <div class="canvas-open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </div>
    
    <div class="hs-nav">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <nav class="nav-menu">
                        <ul>
                            <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/')}}">Home</a></li>
                            <li class="{{ Request::is('authentication-service') || request()->is('search-brand') ? 'active' : '' }}"><a href="{{ url('authentication-service')}}">Jasa Otentikasi</a>
                            @if(isset(Auth::user()->email))
                            <li class="{{ Request::is('lc-history') || request()->is('lc-history-view*') ? 'active' : '' }}"><a href="{{ url('lc-history')}}">My LC History</a>
                            @endif
                                <!-- <ul class="dropdown">
                                    <li><a href="./property.html">Property Grid</a></li>
                                    <li><a href="./profile.html">Property List</a></li>
                                    <li><a href="./property-details.html">Property Detail</a></li>
                                    <li><a href="./property-comparison.html">Property Comperison</a></li>
                                    <li><a href="./property-submit.html">Property Submit</a></li>
                                </ul> -->
                            </li>
                            <li class="{{ Request::is('legit-check-history') || request()->is('legit-check-history-detail*') ? 'active' : '' }}"><a href="{{ url('legit-check-history')}}">History</a>
                            <li class="{{ Request::is('about-us-page') ? 'active' : '' }}"><a href="{{ url('about-us-page')}}">About Us</a>
                            <li class="{{ Request::is('article-page') || request()->is('article-page-details*') ? 'active' : '' }}"><a href="{{ url('article-page')}}">Artikel</a>
                            <!-- <li><a href="./agents.html">Agents</a></li>
                            <li><a href="./about.html">About</a></li>
                            <li><a href="./blog.html">Blog</a></li>
                            <li><a href="./contact.html">Contact</a></li> -->
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="hn-social">
                        <a href="{{$page_config->instagram_account}}" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="{{$page_config->whatsapp_api}}" target="_blank"><i class="fa fa-whatsapp"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Keranjang Belanja</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if(Session::has('cart'))
            <div style="overflow-x:auto;">
                <table id="classTable" class="table table-borderless">
                    <thead>
                    </thead>
                    <tbody>
                    @foreach (Session::get('cart')->items as $cartItems)
                        <tr>
                            <td><a href="{{ route('items.removeFromCart', ['id' => $cartItems['item']['id'] ]) }}" class="remove-product-link w-inline-block"></a></td>
                            <td><img src="{{$cartItems['item']['image_url']}}" style="max-height:50px;max-width:72px;"></td>
                            <td>{{$cartItems['item']['title']}}</td>
                            <td>{{$cartItems['qty'] .' X '. \NumberUtil::convertNumberToRupiah($cartItems['item']['price'])}}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Subtotal</td>
                            <td>{{\NumberUtil::convertNumberToRupiah(Session::get('cart')->totalPrice)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div> 
            @else
            <p>Anda belum memiliki jasa otentikasi apapun. Ayo mulai belanja</p>
            @endif
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        @if(Session::has('cart'))
            <a href="{{ url('checkout') }}" class="btn btn-sendiri">Checkout</a>
        @else
            <a class="btn btn-sendiri" disabled>Checkout</a>
        @endif
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>

</header>
<!-- Header End -->

</html>