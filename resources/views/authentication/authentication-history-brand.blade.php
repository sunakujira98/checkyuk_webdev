<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Checkyuk Template">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - History Legit Check</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">
    <style>
    .top-right {
        position: absolute;
        top: 8px;
        right: 16px;
    }
    </style>
</head>
<body>
    @include('includes.header')


    <div class="container mt-5">
        @include('includes.flash-message')

        @yield('content')
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1" style="font-size: 2rem;font-weight: bold;">CARI TAHU KEASLIAN PRODUK ANDA! </h3>
                <h4 data-aos="fade-up" class="mb-1" style="font-size: 1.5rem;">Malu dong kalau punya produk mahal tapi dikira palsu? Yuk, kita bantu tentuin keaslian produk lo!! </h4>
                </div>
            </div>

            </div>
        </div>
    </div>

    <!-- Contact Form Section Begin -->
    <section class="contact-form-section spad mt-n5">
        <div class="popular_destination_area">
            <div class="container">
                <div class="cf-content">
                    <div class="cc-title">
                        <h4>RIWAYAT PEMERIKSAAN BRAND {{$items->title}}</h4>
                    </div>
                    <div class="row mb-5">
                        <div class="col-md-12" style="text-align:right;">
                        <form action="{{route ('search.lc')}}" method="POST" role="search">
                        {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="Cari kode LC ..." autocomplete="off">
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn btn" style="background:#00C89E;color:white;">
                                    <span class="fa fa-search"></span>
                                    </button>
                                </span>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($transactionDetails as $detail)
                    <div class="col-md-3">
                        <div class="property-item">
                            <div class="single_destination" style="margin-bottom:-5px;">
                                <div class="thumb">
                                    <a href="{{ route('history.ResultDetailView', ['id' => $detail['id'], 'name' => $detail->product_name]) }}">
                                    <div class="pi-pic set-bg" data-setbg="{{ asset($detail->transaction_details_images[0]->image_url)}}" style="max-width:300px;max-height:300px;background-repeat: no-repeat;background-position: center center;background-size: cover;">
                                        @if($detail->is_original === NULL)
                                            <div class="label c-blue">PROSES PENGECEKAN</div>
                                        @elseif ($detail->is_original === 1)
                                            <div class="label c-tosca">AUTHENTIC</div>
                                        @elseif ($detail->is_original === 0)
                                            <div class="label c-orange">FAKE</div>
                                        @elseif ($detail->is_original === 2)
                                            <div class="label c-orange">INDEFINEABLE</div>
                                        @endif
                                    </div>
                                    </a>
                                </div>
                            </div>
                            <div class="pi-text" style="text-align:center">
                                <div class="pt-price">{{$detail->product_name}}<span></span></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @if(!$transactionDetails->isEmpty())
                <div>
                    {{$transactionDetails->links()}}
                </div>
                @endif
            </div>
        </div>
    </section>
    <!-- Contact Form Section End -->

    @include('includes.whychooseus')
    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>