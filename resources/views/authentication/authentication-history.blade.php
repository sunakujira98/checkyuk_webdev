<!DOCTYPE html>
<html lang="zxx">
<head>
<meta charset="UTF-8">
    <meta name="description" content="Checkyuk Template">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - History Legit Check</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">
</head>
<body>
    @include('includes.header')


    <div class="container mt-5">
        @include('includes.flash-message')

        @yield('content')
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1" style="font-size: 2rem;font-weight: bold;">{{$page_config->history_caption1}} </h3>
                <h4 data-aos="fade-up" class="mb-1" style="font-size: 1.5rem;">{{$page_config->history_caption2}} </h4>
                </div>
            </div>

            </div>
        </div>
    </div>

    <!-- <div class="container mt-5">
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                    <img src="{{ asset('uploads/checkyuk-1-xd.jpg')}}" alt="">
                </div>
            </div>

            </div>
        </div>
    </div> -->

    <!-- Contact Form Section Begin -->
    <section class="contact-form-section spad mt-n5">
        <div class="popular_destination_area">
            <div class="container">
                <div class="cf-content">
                    <div class="cc-title">
                        <h4>PILIH BRAND UNTUK CEK HASIL AUTENTIKASI</h4>
                    </div>
                </div>
                <div class="row">
                @foreach($items as $item)
                    <div class="col-md-3">
                        <div class="single_destination">
                            <div class="thumb card-items">
                                <a href="{{ route('history.Result', ['id' => $item->id, 'name' => $item->title])}}"><img src="{{$item->image_url}}" alt=""></a>
                            </div>
                            <div class="content">
                                <p class="d-flex align-items-center"></p>
                            </div>
                        </div>
                        </a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Form Section End -->
    
    @include('includes.whychooseus')
    @include('includes.footer')
    

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>