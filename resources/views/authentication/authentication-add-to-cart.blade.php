<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Checkout | Checkyuk</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Adminlte -->
  <link rel="stylesheet" href="{{ asset('css/adminlte/adminlte.min.css')}}">
  <!-- fileinput -->
  <link rel="stylesheet" href="{{ asset('css/fileinput/fileinput.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/fileinput/theme.min.css')}}">
  <style>
    .main_section{
      padding: 20px;
      background: #fff;
      box-shadow = 0 0 20px #c1c1c1;
    }
  </style>
</head>
<body>
    @include('includes.header')

    <!-- Main content -->
    <section class="content">
      <div class="container mt-5">
      @include('includes.flash-message')

      @yield('content')
        <!-- form start -->
        {!! Form::open(['route' => 'transaction.placeOrder', 'method' => 'post', 'id' => 'form-billing', 'files' => 'true', 'enctype' => 'multipart/form-data'] ) !!}
        <div class="row mb-5">
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header" style="background-color:#00C89E;">
                <h3 class="card-title">Checkyuk Legitcheck</h3>
              </div>
              <!-- /.card-header -->
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <div class="card-items">
                        <img src="{{$item->image_url}}" alt="" style="max-height:180px;">
                    </div>
                    <div class="card-items">
                        <img src="{{$item->image_url}}" alt="" style="max-height:180px;">
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputPassword1">SANDI</label>
                    <input type="password" class="form-control" id="field-password" name="field-password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">KONFIRMASI SANDI</label>
                    <input type="password" class="form-control" id="field-confirm-password" name="field-confirm-password" placeholder="Password">
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
            </div>
            <!-- /.card -->

            @php
              $indexItem = 0;
            @endphp

              <!-- general form elements -->
              <div class="card">
                <div class="card-header">
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                  <div class="card-body mainsection">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Produk</label>
                    </div>
                    <div class="form-group">                   
                      <p style="margin-bottom:0px;"><b>*File akan terupload ketika melakukan PLACE ORDER</b>
                      <p><b>*Mohon pilih gambar secara bersamaan</b>
                      <input type="hidden" name="_token" value=" {{ csrf_token() }}">
                      <input type="file" id="file-{{$indexItem}}" multiple name="image-{{$indexItem}}[]" class="file" data-overwrite-initial="false" data-msg-placeholder="Pilih Gambar Max 30">
                    </div> 
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                  </div>
              </div>
              <!-- /.card -->
              @php
                $indexItem++;
              @endphp
          </div>
          <!--/.col (left) -->

          <div class="col-md-2">
            
          </div>
          <!--/.col (left) -->
          
          <!-- right column -->
          <div class="col-md-5">
            <!-- sticky top -->
              <div class="card">
                <div class="card-header" style="background-color:#00C89E;">
                  <h3 class="card-title">ORDER DETAILS</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                          <label>PRODUCT</label>
                          @foreach (Session::get('cart')->items as $cartItems)
                          <p>{{$cartItems['item']['title']}}<strong> X {{$cartItems['qty']}}</strong></p>
                          @endforeach
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>TOTAL</label>
                          @foreach (Session::get('cart')->items as $cartItems)
                          <p>{{\NumberUtil::convertNumberToRupiah($cartItems['item']['price'] * $cartItems['qty'])}}</p>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                          <label>GRAND TOTAL</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>{{\NumberUtil::convertNumberToRupiah(Session::get('cart')->totalPrice)}}</label>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <div class="card">
                <div class="card-header" style="background-color:#00C89E;">
                  <h3 class="card-title">BUKTI PEMBAYARAN</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- text input -->
                        <div class="form-group">
                          <label>Tata cara pembayaran checkyuk Legit Check : </label>
                          <p>1. Pembayaran melalui <strong>BCA</strong> Nomor rekening 123123132? AN <strong>STEVEN</strong><p>
                          <p>2. Transfer sesuai nominal total dengan kode unik<p>
                          <p>3. Upload bukti pembayaran dibawah<p>
                          <input type="file" id="file-buktipembayaran" name="image-buktipembayaran" class="file" data-overwrite-initial="false" data-msg-placeholder="Bukti Pembayaran ... ">
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- general form elements disabled -->
              <div class="card card-secondary">
                <!-- /.card-header -->
                <div class="card-body">
                  <button type="submit" class="btn btn-block btn-sendiri btn-lg"><div style="font-weight:550">PLACE ORDER</div></button>
                </div>
                {!! Form::close() !!}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  <!-- Js Plugins -->
  <script src="{{ asset('js/jquery.min.js')}}"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{ asset('js/mixitup.min.js')}}"></script>
  <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
  <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
  <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
  <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
  <script src="{{ asset('js/image-uploader.min.js')}}"></script>
  <script src="{{ asset('js/main.js')}}"></script>
</body>
</html>