<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <title>Checkyuk - History Legit Check Detail</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{ asset('css/fileinput/fileinput.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/fileinput/theme.min.css')}}">
</head>
<body>
    @include('includes.header')


    <div class="container">
        @include('includes.flash-message')

        @yield('content')
    </div>

    @foreach($transactionDetails as $detail)
    <!-- Informasi transaksi begin -->
    <section class="profile-section spad mt-5" style="padding-top:0px;">
        <div class="container">
            <div class="cf-content border-b">
                <div class="cc-title">
                        @if($detail->is_original===NULL)
                        <h5><div style="background:#24A0ED;display:inline;">HASIL : PROSES PENGECEKAN</div></h5>
                        @elseif ($detail->is_original === 1)
                        <h5><div style="background:#5cb85c;display:inline;">HASIL : AUTHENTIC</div></h5>
                        @elseif ($detail->is_original === 0)
                        <h5><div style="background: #f0ad4e;display:inline;">HASIL : FAKE</div> </h5>
                        @elseif ($detail->is_original === 2)
                        <h5><div style="background: #f0ad4e;display:inline;">HASIL : INDEFINEABLE</div> </h5>
                        @endif
                    </h4>
                    <h5>CheckYUK!</h5>
                    <h5>YOUR DAILY LEGITCHECKER!</h5><h5>LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!</h5>
                    <div class="text-center">
                        <img src="{{ asset($detail->items->image_url)}}" alt="" style="max-width:250px;max-width:250px;">
                    </div>
                    <div class="mb-n5" style="text-align:center!important">
                        @if($detail->is_original===NULL)
                        <img src="{{asset ('uploads/symbol/WIP.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 1)
                        <img src="{{asset ('uploads/symbol/AUTHENTIC.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 0)
                        <img src="{{asset ('uploads/symbol/FAKE.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 2)
                        <img src="{{asset ('uploads/symbol/INDEFINEABLE.png')}}" alt="error" style="max-width:160px;">
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="profile-agent-content">
                <div class="row mt-n4">
                    <div class="col-xs-6 col-md-6">
                        <div class="profile-agent-info">
                            <div class="pi-text">
                                <label style="font-weight:bold;">Kategori : </label> {{$detail->items->items_category->title}}<br>
                                <label style="font-weight:bold;">#LegitCheck : </label> {{$detail->id_legit_check}}<br>
                                @if($detail->images_is_not_clear==1)
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-fotoKurangJelas-{{$detail->id}}">UPLOAD FOTO KURANG JELAS</button>
                                @endif   
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <div class="profile-agent-info">
                            <div class="pi-text">
                                <label style="font-weight:bold;">Tanggal Pelayanan : </label> {{$detail->transaction->created_at}}<br>
                                <label style="font-weight:bold;">Nama Produk : </label> {{$detail->product_name}}<br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Informasi transaksi end -->

    <!-- Property Section Begin -->
    <section class="property-section profile-page spad" style="padding-bottom:10px">
        <div class="container">
            <div class="cf-content">
                <div class="cc-title mb-2">
                    <h5>Foto yang dikirim oleh customer</h5>
                    <h5 style="font-size:10px;">*klik gambar untuk memperbesar</h5>
                </div>
            </div>
            <div class="row">
                @foreach($detail->transaction_details_images as $image)
                    <div class="col-md-3">
                        <div class="property-item">
                            <div class="single_destination">
                                <div class="thumb border-gallery center-image">
                                    <a href="{{asset ($image->image_url)}}" data-lightbox="image-{{$detail->id}}">
                                        <img src="{{ asset($image->image_url)}}" style="max-width:250;max-height:250px;background-repeat: no-repeat;background-position: center center;background-size: cover;" alt="Masalah saat memuat gambar">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <label>Komentar dari admin : </label><br>
                    @if ($detail->comment_from_admin==NULL)
                        Admin tidak memberikan komentar.
                    @else
                        {{$detail->comment_from_admin}}
                    @endif
                </div>
            </div>
            <hr>
        </div>
    </section>
    <!-- Property Section End -->
    @endforeach
    <section class="property-section profile-page spad">
        <div class="container">
            {{$transactionDetails->links()}}
        </div>
    </section>

    @include('includes.footer')

    @foreach($transactionDetails as $detail)
    <div class="modal fade bd-example-modal-lg" id="modal-fotoKurangJelas-{{$detail->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Upload foto tambahan {{$detail->product_name}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div style="overflow-x:auto;">
                <div class="form-group">                   
                    <p style="margin-bottom:0px;"><b>*File akan terupload ketika melakukan SIMPAN</b>
                    <p><b>*Mohon pilih SELURUH GAMBAR secara bersamaan.</b>
                    {!! Form::open(['url' => '/upload-missing-images/'.$detail->id , 'method' => 'post', 'id' => 'form-missingImages', 'files' => 'true', 'enctype' => 'multipart/form-data'] ) !!}
                    <input type="hidden" name="_token" value=" {{ csrf_token() }}">
                    <input type="file" id="file-{{$loop->iteration}}" multiple name="image-fotoKurangJelas[]" class="file" data-overwrite-initial="false" data-msg-placeholder="Pilih Gambar Max 30" required>
                    <!-- <label for="keterangan-fotoKurangJelas" class="mt-4">Keterangan</label>
                    <textarea name="keterangan-fotoKurangJelas" class="form-control" rows="3" maxlength="255" placeholder="Keterangan tambahan (Opsional) ... Store pembelian, Rep box, Repainted Boost, Jastip, Harga beli, No laces, dll..."></textarea> -->
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" onclick="this.disabled=true;this.form.submit()" class="btn btn-primary">Simpan</button>
            {!! Form::close() !!}
            </form>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endforeach

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/fileinput.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/themes/fa/theme.min.js"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>

    <script type="text/javascript">
        $('#file-1').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-2').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-3').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-4').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-5').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-6').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-7').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-8').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-9').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });

        $('#file-10').fileinput({
        theme:'fa',
        deleteUrl:"",
        allowedFileExtensions:['jpg', 'png', 'jpeg'],
        overwriteInitial:false,
        dropZoneEnabled: false,
        maxFileSize:20000,
        maxFileCount:30,
        showUpload:false,
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
        },
        slugCallback:function (filename) {
            return filename.replace('(', '_').replace("]",'_');
        }
        
        });
    </script>
</body>

</html>