<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Checkyuk Template">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Jasa Otentikasi</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">
</head>
<body>
    @include('includes.header')


    <div class="container mt-5">
        @include('includes.flash-message')

        @yield('content')
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1" style="font-size: 2rem;font-weight: bold;">CARI TAHU KEASLIAN PRODUK ANDA </h3>
                <h4 data-aos="fade-up" class="mb-1" style="font-size: 1.5rem;">Malu dong kalau punya produk mahal tapi dikira palsu? Yuk, kita bantu tentuin keaslian produk lo!! </h4>
                </div>
            </div>
            </div>
        </div>
    </div>

    <!-- Contact Form Section Begin -->
    <section class="contact-form-section spad mt-n5" style="padding-top:50px;">
        <div class="popular_destination_area">
            <div class="container">
                <div class="cf-content">
                    <div class="cc-title">
                        <h4>Pilih Brand Produk Anda Untuk Mulai Pengecekan</h4>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md-12" style="text-align:right;">
                    <form action="{{route ('search.brand')}}" method="GET" role="search">
                    {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Cari brand... bape/nike/offwhite" autocomplete="off">
                                <span class="input-group-btn">
                                <button type="submit" class="btn btn" style="background:#00C89E;color:white;">
                                <span class="fa fa-search"></span>
                                </button>
                            </span>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    @foreach($items as $item)
                        <div class="col-md-3">
                            <div class="single_destination">
                                <div class="thumb card-items">
                                    <a data-toggle="modal" data-target="#modal-default-{{$item->id}}" style="cursor: pointer;margin-right:-20px;"><img src="{{$item->image_url}}" alt="" style="max-height:180px;"></i></a>
                                    <!-- <a href="{{ route('items.addToCart', ['id' => $item->id ])}}"><img src="{{$item->image_url}}" alt="" style="max-height:180px;"></a> -->
                                </div>
                                <div class="content">
                                    <p class="d-flex align-items-center"></p>
                                </div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                    @if(isset($search))
                    <div class="col-md-12 mt-2">
                    <a href="{{ url('authentication-service')}}" class="btn btn-pill" style="background:#00C89E;color:white;font-weight:bold;">LIHAT SEMUA BRAND</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Form Section End -->
    

    <div class="container mt-n5">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10">
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                    <img src="{{ asset('uploads/checkyuk-1-xd.jpg')}}" alt="">
                </div>
            </div>
            </div>
        </div>
    </div>

    @include('includes.whychooseus')
    
    @foreach($items as $item)
    <div class="modal fade" id="modal-default-{{$item->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Jasa Otentikasi {{$item->title}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div style="overflow-x:auto;">
                <table id="classTable" class="table table-borderless">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>Nama Brand</td>
                            <td>Harga Otentikasi</td>
                        </tr>
                        <tr>
                            <td><img src="{{$item->image_url}}" style="max-height:50px;max-width:72px;"></td>
                            <td>{{$item->title}}</td>
                            <td>{{\NumberUtil::convertNumberToRupiah($item->price)}} (Per Otentikasi)</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><label>QTY</label></td>
                            <form action="{{ route('items.addToCart', ['id' => $item->id ])}}" method="GET">
                            <td><input type="number" min="1" class="form-control" value="1" style="width:50%;" name="qty"></td>
                        </tr>
                    </tbody>
                </table>
            </div> 
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-sendiri">Tambah Ke Keranjang</button>
            </form>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endforeach

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>