<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <title>Checkyuk - History Legit Check Detail</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
</head>
<body>
    @include('includes.header')


    <div class="container">
        @include('includes.flash-message')

        @yield('content')
    </div>

    @foreach($transactionDetails as $detail)
    <!-- Informasi transaksi begin -->
    <section class="profile-section spad mt-5" style="padding-top:0px;">
        <div class="container">
            <div class="cf-content border-b">
                <div class="cc-title">
                        @if($detail->is_original===NULL)
                        <h5><div style="background:#24A0ED;display:inline;">HASIL : PROSES PENGECEKAN</div></h5>
                        @elseif ($detail->is_original === 1)
                        <h5><div style="background:#5cb85c;display:inline;">HASIL : AUTHENTIC</div></h5>
                        @elseif ($detail->is_original === 0)
                        <h5><div style="background: #f0ad4e;display:inline;">HASIL : FAKE</div> </h5>
                        @elseif ($detail->is_original === 2)
                        <h5><div style="background: #f0ad4e;display:inline;">HASIL : INDEFINEABLE</div> </h5>
                        @endif
                    </h4>
                    <h5>CheckYUK!</h5>
                    <h5>YOUR DAILY LEGITCHECKER!</h5><h5>LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!</h5>
                    <div class="text-center">
                        <img src="{{ asset($detail->items->image_url)}}" alt="" style="max-width:250px;max-width:250px;">
                    </div>
                    <div class="mb-n5" style="text-align:center!important">
                        @if($detail->is_original===NULL)
                        <img src="{{asset ('uploads/symbol/WIP.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 1)
                        <img src="{{asset ('uploads/symbol/AUTHENTIC.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 0)
                        <img src="{{asset ('uploads/symbol/FAKE.png')}}" alt="error" style="max-width:160px;">
                        @elseif ($detail->is_original === 2)
                        <img src="{{asset ('uploads/symbol/INDEFINEABLE.png')}}" alt="error" style="max-width:160px;">
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="profile-agent-content">
                <div class="row mt-n4">
                    <div class="col-xs-6 col-md-6">
                        <div class="profile-agent-info">
                            <div class="pi-text">
                                <label style="font-weight:bold;">Kategori : </label> {{$detail->items->items_category->title}}<br>
                                <label style="font-weight:bold;">#LegitCheck : </label> {{$detail->id_legit_check}}<br>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <div class="profile-agent-info">
                            <div class="pi-text">
                                <label style="font-weight:bold;">Tanggal Pelayanan : </label> {{$detail->transaction->created_at}}<br>
                                <label style="font-weight:bold;">Nama Produk : </label> {{$detail->product_name}}<br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Informasi transaksi end -->

    <!-- Property Section Begin -->
    <section class="property-section profile-page spad">
        <div class="container">
            <div class="cf-content">
                <div class="cc-title mb-2">
                    <h5>Foto yang dikirim oleh customer</h5>
                    <h5 style="font-size:10px;">*klik gambar untuk memperbesar</h5>
                </div>
            </div>
            <div class="row">
                @foreach($detail->transaction_details_images as $image)
                    <div class="col-md-3">
                        <div class="property-item">
                            <div class="single_destination">
                                <div class="thumb border-gallery center-image">
                                    <a href="{{asset ($image->image_url)}}" data-lightbox="image-{{$detail->id}}">
                                        <img src="{{ asset($image->image_url)}}" style="max-width:250;max-height:250px;background-repeat: no-repeat;background-position: center center;background-size: cover;" alt="Masalah saat memuat gambar">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Property Section End -->
    @endforeach

    @include('includes.whychooseus')

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>
</body>

</html>