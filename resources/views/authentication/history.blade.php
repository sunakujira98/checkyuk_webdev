<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
    <title>Checkyuk - History Legit Check</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/scroller/2.0.1/css/scroller.jqueryui.min.css')}}">
    
    
</head>
<body>
    @include('includes.header')


    <div class="container mt-5">
        @include('includes.flash-message')

        @yield('content')
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1" style="font-size: 2rem;font-weight: bold;">{{$page_config->mylchistory_caption1}} </h3>
                <h4 data-aos="fade-up" class="mb-1" style="font-size: 1.5rem;">{{$page_config->mylchistory_caption2}}</h4>
                </div>
            </div>

            </div>
        </div>
    </div>

    <!-- Contact Form Section Begin -->
    <section class="contact-form-section spad mt-n5">
        <div class="popular_destination_area">
            <div class="container">
                <div class="cf-content">
                    <div class="cc-title">
                        <h4>History Legit Check</h4>
                    </div>
                </div>
                <table id="table-branches" class="ui unstackable celled table striped" style="width: 100%;overflow-x:auto;">
                    <thead>
                        <th>No. </th>
                        <th>Tanggal Legit Check</th>
                        <th>Qty Legit Check</th>
                        <th>Total Legit Check</th>
                        <th>Bukti Pembayaran</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$transaction->created_at->format('d-M-Y')}}</td>
                            <td>{{$transaction->total_qty}} pcs</td>
                            <td>{{\NumberUtil::convertNumberToRupiah($transaction->grand_total)}}</td>
                            <td>
                                <a href="{{$transaction->payment_proof_image}}" data-lightbox="image-{{$loop->iteration}}">
                                    <img src="{{$transaction->payment_proof_image}}" alt="error load image" heigth="40px;" width="40px;">
                                </a>
                            </td>
                            <td><a href="{{ route('lc-history.viewDetail', ['id' => $transaction->id ]) }}">Lihat Detail</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- Contact Form Section End -->

    @include('includes.whychooseus')

    @include('includes.footer')

    <!-- Js Plugins -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/mixitup.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
    <script src="{{ asset('js/image-uploader.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
            var dataTable = $('#table-branches').DataTable({
                paging: true,
                pageLength: 15,
                searching: true,
                info: true,
                dom: 'lfBtp',
                scrollX: true,
            });
            dataTable.on( 'order.dt search.dt', function () {
                dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = (i+1) + ".";
                dataTable.cell(cell).invalidate('dom'); 
                });
            }).draw();
    });
    </script>
</body>

</html>