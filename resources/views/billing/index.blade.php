<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="Checkyuk, LEGIT CHECKER, HYPEBEAST INDONESIA, LC,  html">
  <title>Checkyuk - Pembayaran</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Adminlte -->
  <link rel="stylesheet" href="{{ asset('css/adminlte/adminlte.min.css')}}">
  <!-- fileinput -->
  <link rel="stylesheet" href="{{ asset('css/fileinput/fileinput.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/fileinput/theme.min.css')}}">
  <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
  
  <style>
    .main_section{
      padding: 20px;
      background: #fff;
      box-shadow = 0 0 20px #c1c1c1;
    }
    .hyperlinkToOrder { text-decoration: none; color:light-blue; }
    a.hyperlinkToOrder:hover { color:gray; }
    a.hyperlinkToOrder:visited { color: blue; }
  </style>
</head>
<body>
    @include('includes.header')

    <!-- Main content -->
    <section class="content">
      <div class="container mt-5">
      @include('includes.flash-message')

      @yield('content')
      @if(empty(Session::get('cart')->items))
      <div class="d-flex justify-content-center">
        Cart anda kosong. Silahkan memesan legit checker produk mana yang anda inginkan di 
        <a class="hyperlinkToOrder" href="{{url('/authentication-service')}}">sini</a>
      </div>
      @else
        <!-- form start -->
        {!! Form::open(['route' => 'transaction.placeOrder', 'method' => 'post', 'id' => 'form-billing', 'files' => 'true', 'enctype' => 'multipart/form-data', 'onsubmit' => 'myButton.disabled => true; return true;'] ) !!}
        <div class="row mb-5">
          <!-- left column -->
          <div class="col-md-7">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">BILLING DETAILS</h3>
              </div>
              <!-- /.card-header -->
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">EMAIL</label>
                    <input type="email" class="form-control" id="field-email" name="field-email" placeholder="Enter email" value="{{Auth::user()->email}}" readonly>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputPassword1">SANDI</label>
                    <input type="password" class="form-control" id="field-password" name="field-password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">KONFIRMASI SANDI</label>
                    <input type="password" class="form-control" id="field-confirm-password" name="field-confirm-password" placeholder="Password">
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
            </div>
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">TATA CARA UPLOAD GAMBAR</h3>
              </div>
              <!-- /.card-header -->
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label>Tata cara upload gambar Checkyuk : </label>
                      <p>1. Dalam memilih file, pilih <strong>SELURUH FILE</strong> secara bersamaan<p>
                      <p>2. Untuk proses hasil yang lebih cepat, harap mengikuti harap mengikuti guideline foto yang tersedia di link<a href="{{url('photos-guide-page')}}"> <b>berikut ini</b></a> <p>
                    </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputPassword1">SANDI</label>
                    <input type="password" class="form-control" id="field-password" name="field-password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">KONFIRMASI SANDI</label>
                    <input type="password" class="form-control" id="field-confirm-password" name="field-confirm-password" placeholder="Password">
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
            </div>
            <!-- /.card -->

            @php
              $indexItem = 0;
            @endphp
            @foreach (Session::get('cart')->items as $cartItems)
              @for ($i = 0; $i < $cartItems['qty']; $i++)
              <!-- general form elements -->
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Informasi <strong>{{$cartItems['item']['title'].' Ke - '.($i+1)}}</strong></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                  <div class="card-body mainsection">
                    <div class="form-group">
                      <label for="field-items-{{$indexItem}}">Nama Produk</label>
                      <input type="text" class="form-control" id="field-items-{{$indexItem}}" name="field-items-{{$indexItem}}" placeholder="Nama Produk {{$cartItems['item']['title'].' Ke - '.($i+1)}}" required autocomplete="off">
                    </div>
                    <div class="form-group">                   
                      <p style="margin-bottom:0px;"><b>*File akan terupload ketika melakukan PLACE ORDER</b>
                      <p><b>*Mohon pilih SELURUH GAMBAR secara bersamaan.</b>
                      <input type="hidden" name="_token" value=" {{ csrf_token() }}">
                      <input type="file" id="file-{{$indexItem}}" multiple name="image-{{$indexItem}}[]" class="file" data-overwrite-initial="false" data-msg-placeholder="Pilih Gambar Max 30" required>
                      <label for="keterangan-{{$indexItem}}" class="mt-4">Keterangan</label>
                      <textarea name="keterangan-{{$indexItem}}" class="form-control" rows="3" maxlength="255" placeholder="Keterangan tambahan (Opsional) ... Store pembelian, Rep box, Repainted Boost, Jastip, Harga beli, No laces, dll..."></textarea>
                    </div> 
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                  </div>
              </div>
              <!-- /.card -->
              @php
                $indexItem++;
              @endphp
              @endfor
            @endforeach
          </div>
          <!--/.col (left) -->
          
          <!-- right column -->
          <div class="col-md-5">
            <!-- sticky top -->
            <div class="sticky-top">
              <div class="card">
                <div class="card-header" style="background-color:#00C89E;">
                  <h3 class="card-title">ORDER DETAILS</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                          <label>PRODUCT</label>
                          @foreach (Session::get('cart')->items as $cartItems)
                          <p>{{$cartItems['item']['title']}}<strong> X {{$cartItems['qty']}}</strong></p>
                          @endforeach
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>TOTAL</label>
                          @foreach (Session::get('cart')->items as $cartItems)
                          <p>{{\NumberUtil::convertNumberToRupiah($cartItems['item']['price'] * $cartItems['qty'])}}</p>
                          @endforeach
                        </div>
                      </div>
                      @php
                        $kodeUnik = rand(10, 99);
                      @endphp
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>KODE UNIK</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <p>{{$kodeUnik}}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                          <label>GRAND TOTAL</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>{{\NumberUtil::convertNumberToRupiah(Session::get('cart')->totalPrice+$kodeUnik)}}</label>
                          <input type="hidden" name="grand_total" value="{{Session::get('cart')->totalPrice+$kodeUnik}}">
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <div class="card">
                <div class="card-header" style="background-color:#00C89E;">
                  <h3 class="card-title">BUKTI PEMBAYARAN</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- text input -->
                        <div class="form-group">
                          <label>Tata cara pembayaran checkyuk Legit Check : </label>
                          <p>1. Pembayaran melalui <strong>BCA</strong> Nomor rekening <strong>8645087891</strong> AN <strong>STEPHANIE</strong><p>
                          <p>2. Transfer sesuai nominal total dengan kode unik<p>
                          <p>3. Upload bukti pembayaran dibawah<p>
                          <p>4. Berikut gambar QR jika ingin melakukan transfer via QR<p>
                          <a href="{{asset ('uploads/qr.jpg')}}" data-lightbox="image-1">
                            <img src="{{asset ('uploads/qr.jpg')}}" style="max-height:400px;margin-bottom:20px;">
                          </a>
                          <input type="file" id="file-buktipembayaran" name="image-buktipembayaran" class="file" data-overwrite-initial="false" data-msg-placeholder="Bukti Pembayaran ... " required>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <div class="card">
                <div class="card-header" style="background-color:#00C89E;">
                  <h3 class="card-title">PLACE ORDER</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- text input -->
                        <div class="form-group">
                          <label>Baca syarat dan ketentuan disini : <a href="/terms-condition-page">Halaman T&C</a></label>
                          <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="agreement" autocomplete="off">
                          <label class="form-check-label" for="defaultCheck1">
                            Saya setuju dengan syarat dan ketentuan yang berlaku di Checkyuk
                          </label>
                          <div class="mt-2">
                            <label>*Pengguna iphone / android mohon tunggu hingga upload selesai, Jangan minimize / tutup page ini, dan jangan berpindah ke aplikasi lain.</label>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- /.card-header -->
              <div class="card-body">
                <button type="submit" class="btn btn-block btn-sendiri btn-lg" disabled onclick="return sendLove(this);" name="myButton"><div style="font-weight:550">PLACE ORDER</div></button>
              </div>
              {!! Form::close() !!}
              <!-- /.card -->
            </div>
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
        @endif
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  <!-- Js Plugins -->
  <script src="{{ asset('js/jquery.min.js')}}"></script>
  <script src="{{ asset('js/fileinput.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/themes/fa/theme.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>
  <script src="{{ asset('js/mixitup.min.js')}}"></script>
  <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
  <script src="{{ asset('js/jquery.nice-select.min.js')}}"></script>
  <script src="{{ asset('js/jquery.slicknav.js')}}"></script>
  <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('js/jquery.richtext.min.js')}}"></script>
  <script src="{{ asset('js/image-uploader.min.js')}}"></script>
  <script src="{{ asset('js/main.js')}}"></script>
  <script type="text/javascript">
    window.onload = function() {};
    window.onunload = function() {};
  </script>
  <script>
    $(document).ready(function(){
        $('#agreement').click(function(){
            if($(this).prop("checked") == true){
              $(".btn-sendiri").prop('disabled', false);
            }
            else if($(this).prop("checked") == false){
              $(".btn-sendiri").prop('disabled', true);
            }
        });
    });
  </script>

  <script>
    function sendLove(this1)
    {
      //alert('asdasd');
      this1.innerHTML='<i class="fa fa-spinner fa-spin"></i> Sedang upload gambar, mohon ditunggu (Jangan minimize / ganti ke aplikasi lain / close page ini) hingga upload selesai...';
    }

    $('#form-billing').submit(function(){
        $(this).find('button[type=submit]').prop('disabled', true);
    });
  </script>

  <script type="text/javascript">
    $('#file-0').fileinput({
      theme:'fa',
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      fileActionSettings: {
          showRemove: false,
          showUpload: false,
      },
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-1').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-2').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-3').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });
    
    $('#file-4').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-5').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-6').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-7').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-8').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-9').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      maxFileCount:30,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });

    $('#file-buktipembayaran').fileinput({
      theme:'fa',
      uploadExtraData:function(){
        return{
          _token:$("input[name='_token']").val()
        };
      },
      deleteUrl:"",
      allowedFileExtensions:['jpg', 'png', 'jpeg', 'gif'],
      overwriteInitial:false,
      dropZoneEnabled: false,
      maxFileSize:20000,
      showUpload:false,
      slugCallback:function (filename) {
        return filename.replace('(', '_').replace("]",'_');
      }
      
    });
  </script>
</body>
</html>
