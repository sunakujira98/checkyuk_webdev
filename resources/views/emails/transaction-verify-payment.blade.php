
@component('mail::message')
<div style="  margin: auto;width: 50%;">
    <a href="{{ url('/')}}">
            <img src="{{ asset('/uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
    </a>
</div>
<br>
<br>
{{-- Greeting --}}
Hi {{$namaUser}}<br>

@if($payment_is_verified == 1)
    {{-- Outro Lines --}}
    Pembayaran anda di website kami berhasil di verifikasi
@else
    {{-- Outro Lines --}}
    Pembayaran anda di website kami gagal di verifikasi 
    Mohon untuk menghubungi Customer Service kami via Whatsapp 08139612102
@endif

{{-- Salutation --}}
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent