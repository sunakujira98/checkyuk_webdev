@component('mail::message')
<div style="  margin: auto;width: 50%;">
        <a href="{{ url('/')}}">
            <img src="{{ asset('/uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
        </a>
</div>
<br>
<br>
Hi! <strong>{{$transactionDetail->transaction->user->name}}</strong>
<br>
<h2>Hasil otentikasi untuk produk {{$transactionDetail->product_name}} telah tersedia dengan kode legit check LC - {{$transactionDetail->transaction->id}} telah tersedia.</h2>
<h3>Mohon akses MY LC HISTORY untuk melihat hasli legit check.</h3>
<table>
	<tr>
		<td>Nama Produk</td>
		<td> </td>
		<td>{{$transactionDetail->product_name}}</td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td> </td>
		<td>{{$transactionDetail->updated_at}}</td>
	</tr>
	<tr>
		<td>Hasil</td>
		<td> </td>
		<td>{{$original}}</td>
	</tr>
</table>

@if($original=="INDEFINEABLE")
Karena hasil adalah INDEFINEABLE, balas email ini untuk melakukan refund atau kontak admin via whatsapp 08139612102.<br>
@endif

Mohon akses MY LC HISTORY (Jangan lupa login dulu yah), kamu bisa lihat LC kamu dengan Lihat Detail pada Transaksi nya.
<a href="https://checkyuk.com/lc-history-view/{{$transactionDetail->transaction_id}}" class="btn btn-primary">Klik disini untuk akses halaman tersebut</a>
<br>
Terima Kasih,<br>
Checkyuk
@endcomponent