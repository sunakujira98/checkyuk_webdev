
@component('mail::message')
<div style="  margin: auto;width: 50%;">
    <a href="{{ url('/')}}">
            <img src="{{ asset('/uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
    </a>
</div>
<br>
<br>
{{-- Greeting --}}
Hi Admin CheckYuk!<br>

{{-- Intro Lines --}}
Baru saja ada user yang melakukan transaki pada checkyuk.com dengan detail :<br>
<table>
	<tr>
		<td>Email User</td>
		<td> </td>
		<td> : {{$user}}</td>
	</tr>
	<tr>
		<td>Total Harga Transaksi : </td>
		<td> </td>
		<td> : Rp{{$totalPrice}}</td>
	</tr>
</table><br>

{{-- Outro Lines --}}
Silahkan untuk login ke dalam website admin checkyuk untuk melakukan pengecekan transaksi.<br>

{{-- Salutation --}}
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent