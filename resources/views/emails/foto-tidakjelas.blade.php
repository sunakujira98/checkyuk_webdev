@component('mail::message')
<div style="  margin: auto;width: 50%;">
        <a href="{{ url('/')}}">
            <img src="{{ asset('/uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" alt="" style="margin-bottom:-35%;margin-top:-30%;">
        </a>
</div>
<br>
<br>
Hi! <strong>{{$transactionDetail->transaction->user->name}}</strong>
<br>
<h2>Sorry nih, foto kamu dari LC {{$transactionDetail->id_legit_check}} ada yang kurang jelas.</h2>
<br>
<h3>Berikut detail untuk foto yang kurang jelasnya.</h3>
<table>
	<tr>
		<td>ID LEGIT CHECK</td>
		<td> </td>
		<td>{{$transactionDetail->id_legit_check}}</td>
	</tr>
	<tr>
		<td>TANGGAPAN ADMIN</td>
		<td> </td>
		<td>{{$transactionDetail->comment_images_is_not_clear}}</td>
	</tr>
</table>
<br>
Mohon akses MY LC HISTORY (Jangan lupa login dulu yah), kamu bisa lihat LC kamu dengan Lihat Detail pada Transaksi nya. Lalu pilih "Upload ulang gambar".
<br><br>
<a href="https://checkyuk.com/lc-history-view/{{$transactionDetail->transaction_id}}" class="btn btn-primary">Klik disini untuk akses halaman tersebut</a>
<br><br>
Terima Kasih,<br>
Checkyuk
@endcomponent