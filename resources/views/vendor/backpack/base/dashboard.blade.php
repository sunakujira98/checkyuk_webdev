@extends(backpack_view('blank'))

@php
    $transaction = App\Models\Transaction::where('payment_is_verified', '!=', 0)->orWhereNull('payment_is_verified')->get('id');
    $details = App\Models\TransactionDetails::where('is_original', '=', NULL)->whereIn('transaction_id', $transaction)->whereNull('images_is_not_clear')->orderBy('created_at', 'ASC')->get();
    $verifikasiPembayaran = App\Models\Transaction::where('payment_is_verified', '=', NULL)->orderBy('created_at', 'ASC')->get();
@endphp
@section('before_styles')
    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@endsection
@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
		{!!$message!!}
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        {!!$message!!}
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        {!!$message!!}
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        {!!$message!!}
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors
</div>
@endif
<h2>PENGECEKAN OTENTIKASI</h2>
<table id="table-branches" class="ui unstackable celled table striped" style="width: 100%;overflow-x:auto;">
		<thead>
			<tr>
                <th class="center aligned">NO</th>
				<th class="center aligned">ID TRANSAKSI</th>
				<th class="center aligned">TGL TRANSAKSI</th>
				<th class="center aligned">NAMA PRODUK</th>
				<th class="center aligned">KOMENTAR CUSTOMER</th>
                <th class="center aligned">KOMENTAR ADMIN</th>
				<th class="center aligned">AUTHENTIC?</th>
				<th class="center aligned">ITEM</th>    
				<th class="center aligned">QTY</th>
                <th class="center aligned">GAMBAR</th>
                <th class="center aligned">ACTION</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($details as $detail)
			<tr>
                <td>{{$loop->iteration}}</td>
				<td>LC-{{$detail->transaction->id}}</td>
				<td>{{$detail->transaction->created_at}}</td>
                <td>{{$detail->product_name}}</td>
				<td>{{$detail->comment}}</td>
				<td>{{$detail->comment_from_admin}}</td>
                <td>
                    @if($detail->is_original === NULL)
                        BELUM DI CEK
                    @elseif($detail->is_original === 1)
                        AUTHENTIC
                    @elseif($detail->is_original === 0)
                        FAKE
                    @elseif($detail->is_original === 2)
                        INDEFINEABLE
                    @endif
                </td>
                <td>{{$detail->items->title}}</td>
                <td>{{$detail->qty}}</td>
                <td>
                    <div class="thumb border-gallery center-image">
                    @foreach($detail->transaction_details_images as $image)
                        <a href="{{asset ($image->image_url)}}" data-lightbox="image-{{$detail->id}}">
                            <img src="{{ asset($image->image_url)}}" style="max-width:50px;max-height:50px;background-repeat: no-repeat;background-position: center center;background-size: cover;" alt="Masalah saat memuat gambar">
                        </a>
                    @endforeach
                    </div>
                </td>
                @if($detail->transaction->payment_is_verified !== NULL)
                    @if($detail->is_original === NULL)
                    <td class="center aligned">
                        <a href="{{ url('admin/transactiondetail/'.$detail->id.'/change-status/1') }}" class="btn btn-sm btn-link">
                            <i class="fa fa-check"></i>
                            ORIGINAL
                        </a>
                        <a href="{{ url('admin/transactiondetail/'.$detail->id.'/change-status/0') }}" class="btn btn-sm btn-link">
                        <i class="fa fa-times"></i>
                            FAKE
                        </a>
                        <a href="{{ url('admin/transactiondetail/'.$detail->id.'/change-status/2') }}" class="btn btn-sm btn-link">
                            INDEFINEABLE
                        </a>
                        @if($detail->comment_from_admin!=NULL)
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-{{$detail->id}}">
                                GANTI KOMENTAR
                            </button>
                        @else
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default-{{$detail->id}}" style="margin-bottom:5px;">
                                TAMBAH KOMENTAR
                            </button>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default2-{{$detail->id}}">
                                FOTO KURANG JELAS
                            </button>    
                        @endif                    
                    </td>
                    @else
                    <td class="center aligned">
                    </td>
                    @endif
                @else
                    <td>MOHON VERIFIKASI PEMBAYARAN TERLEBIH DAHULU</td>
                @endif
    		</tr>
		@endforeach
		</tbody>
</table>

<hr>
<h2>VERIFIKASI PEMBAYARAN DI BAWAH INI</h2>
<table id="table-branches2" class="ui unstackable celled table striped" style="width: 100%;">
		<thead>
			<tr>
                <th class="center aligned">NO</th>
				<th class="center aligned">ID TRANSAKSI.</th>
				<th class="center aligned">TGL TRANSAKSI</th>
				<th class="center aligned">EMAIL CUSTOMER</th>
				<th class="center aligned">TOTAL TAGIHAN</th>
				<th class="center aligned">BUKTI PEMBAYARAN</th>
				<th class="center aligned">PEMBAYARAN SESUAI?</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($verifikasiPembayaran as $pembayaran)
			<tr>
                <td>{{$loop->iteration}}</td>
				<td>LC-{{$pembayaran->id}}</td>
                <td>{{$pembayaran->created_at->format('d-M-Y')}}</td>
                <td>{{$pembayaran->user->email}}</td>
                <td>{{\NumberUtil::convertNumberToRupiah($pembayaran->grand_total)}}</td>
                <td>
                    <div class="thumb border-gallery center-image">
                        <a href="{{asset ($pembayaran->payment_proof_image)}}" data-lightbox="image-{{$pembayaran->id}}">
                            <img src="{{ asset($pembayaran->payment_proof_image)}}" style="max-width:50px;max-height:50px;background-repeat: no-repeat;background-position: center center;background-size: cover;" alt="Masalah saat memuat gambar">
                        </a>
                    </div>
                </td>
                @if($pembayaran->payment_is_verified === NULL)
                <td class="center aligned">
                    <a href="{{ url('admin/transaction/'.$pembayaran->id.'/verify-payment/1') }}" class="btn btn-sm btn-link">
                        <i class="fa fa-check"></i>
                        VERIFIED
                    </a>
                    <a href="{{ url('admin/transaction/'.$pembayaran->id.'/verify-payment/0') }}" class="btn btn-sm btn-link">
                      <i class="fa fa-times"></i>
                        GAGAL
                    </a>
                </td>
                @else
                <td class="center aligned">
                    <a href="{{ url('admin/transaction/'.$pembayaran->id.'/emailCustomerVerifyPayment') }} " class="btn btn-sm btn-link">
                    <i class="fa fa-envelope"></i>
                      Kirim Email
                    </a>
                </td>
                @endif
    		</tr>
		@endforeach
		</tbody>
</table>
@endsection

@foreach($details as $detail)
    <div class="modal fade" id="modal-default-{{$detail->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Jasa Otentikasi {{$detail->id}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div style="overflow-x:auto;">
                <table id="classTable" class="table table-borderless">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td><label>KOMENTAR</label></td>
                            <form action="{{url('admin/transactiondetail/'.$detail->id.'/add-comments')}}" method="GET">
                            <td><textarea name="komentar" class="form-control" rows="5" maxlength="255" placeholder="Keterangan kepada customer">{{$detail->comment_from_admin}}</textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div> 
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-default2-{{$detail->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Foto kurang jelas untuk jasa Otentikasi {{$detail->id}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div style="overflow-x:auto;">
                <table id="classTable" class="table table-borderless">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td><label>KETERANGAN</label></td>
                            <form action="{{url('admin/transactiondetail/'.$detail->id.'/foto-tidakjelas/1')}}" method="GET">
                            <td><textarea name="keteranganfoto" class="form-control" rows="5" maxlength="255" placeholder="Keterangan deskripsi foto yang tidak jelas kepada customer (Ex : Insole, Laces)">{{$detail->comment_from_admin}}</textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div> 
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endforeach

@section('after_scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('vendor/lightbox/js/lightbox.js')}}"></script>
<script type="text/javascript">
 $(document).ready(function() {
        var dataTable = $('#table-branches').DataTable({
            paging: true,
            pageLength: 15,
            searching: true,
            info: true,
            dom: 'lfBtp',
            scrollX: true,
        });
        dataTable.on( 'order.dt search.dt', function () {
            dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = (i+1) + ".";
               dataTable.cell(cell).invalidate('dom'); 
            });
        }).draw();
});

$(document).ready(function() {
        var dataTable = $('#table-branches2').DataTable({
            paging: true,
            pageLength: 15,
            searching: true,
            info: true,
            dom: 'lfBtp',
            scrollX: true,
        });
        dataTable.on( 'order.dt search.dt', function () {
            dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
               cell.innerHTML = (i+1) + ".";
               dataTable.cell(cell).invalidate('dom'); 
            });
        }).draw();
});
</script>
@endsection