<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-truck"></i> Items</a>
    <ul class="nav-dropdown-items">
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('item') }}"><i class="nav-icon fa fa-truck"></i> <span>Items</span></a></li>
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('item-category') }}"><i class="nav-icon fa fa-list"></i> <span>Categories</span></a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-exchange"></i> Transactions</a>
    <ul class="nav-dropdown-items">
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('transaction') }}"><i class="nav-icon fa fa-exchange"></i> <span>Transaction</span></a></li>
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('transactiondetail') }}"><i class="nav-icon fa fa-info"></i> <span>Details</span></a></li>
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('transactiondetailimage') }}"><i class="nav-icon fa fa-picture-o"></i> <span>Images</span></a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-newspaper-o"></i>News</a>
    <ul class="nav-dropdown-items">
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}"><i class="nav-icon fa fa-newspaper-o"></i> Articles</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('category') }}"><i class="nav-icon fa fa-list"></i> Categories</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('tag') }}"><i class="nav-icon fa fa-tag"></i> Tags</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-cog"></i> Settings</a>
    <ul class="nav-dropdown-items">
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('aboutussettings') }}'><i class='nav-icon fa fa-home'></i> About Us</a></li>
		<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('homesetting') }}'><i class='nav-icon fa fa-home'></i> Home</a></li> -->
		<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('servicesetting') }}'><i class='nav-icon fa fa-wrench'></i> Service</a></li> -->
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('whychooseussetting') }}'><i class='nav-icon fa fa-question'></i> WhyChooseUs</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('refundpolicysettings') }}'><i class='nav-icon fa fa-question'></i> Refund Policy</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('termsconditionpolicysettings') }}'><i class='nav-icon fa fa-question'></i> TC Policy</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('guidephotossettings') }}'><i class='nav-icon fa fa-question'></i> Photo Guides</a></li>
		<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('footersetting') }}'><i class='nav-icon fa fa-copyright'></i> Footer</a></li> -->
    </ul>
</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('pageconfig') }}'><i class='nav-icon fa fa-file'></i>PageConfigs</a></li>