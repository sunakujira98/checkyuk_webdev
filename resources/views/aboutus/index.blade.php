<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Checkyuk, LegitCheck, LegitChecker, ShoeAuthentication">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkyuk - Tentang Kita</title>
    <link href = "{{ asset('uploads/CHECKYUK_LOGO_TRANSPARANT.png')}}" rel="icon" type="image/gif">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    @include('includes.header')

    <div class="site-blocks-cover overlay" style="background-image: url({{$page_config->aboutus_background_image}});" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div style="padding-top:5%;">
            @include('includes.flash-message')
        </div>
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10">
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-10 text-center">
                <h3 data-aos="fade-up" class="mb-1">{{$page_config->aboutus_caption1}} </h3>
                <h4 data-aos="fade-up" class="mb-1">{{$page_config->aboutus_caption2}} </h4>
                <h4 data-aos="fade-up" class="mb-5">{{$page_config->aboutus_caption3}} </h4>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- About Section Begin -->
    <section class="about-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-text">
                        <div class="at-title">
                            <h3>TENTANG CHECKYUK</h3>
                        </div>
                        <div class="at-feature">
                            @foreach($about_us->sortBy('order') as $about)
                            <div class="af-item">
                                <div class="af-text">
                                    <h6>{{$about->judul}}</h6>
                                    <p>{{$about->caption}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-pic set-bg" data-setbg="{{$page_config->aboutus_image}}">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    @include('includes.footer')
    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.richtext.min.js"></script>
    <script src="js/image-uploader.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>