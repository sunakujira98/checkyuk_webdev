<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
        [
            'email' => 'checkyuk.id@gmail.com',
            'name' => 'Checkyuk Admin',
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'password' => bcrypt("checkyuk123"),
            'phone_number' => '081212341234',
        ]
        ]);
}
}
