<?php

use Illuminate\Database\Seeder;

class AboutUsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('about_us_settings')->insert([
        [
            'judul' => 'WELCOME TO CHECKYUK',
            'caption' => 'Checkyuk adalah platform pengecekan hype stuff berfokus pada sepatu yang berdiri sejak tahun 2019.',
            'order' => 1,
        ],
        [
            'judul' => 'CHECKYUK',
            'caption' => 'Checkyuk adalah platform untuk para pecinta HYPE yang SANGAT MENGHARGAI authenticity (keaslian) suatu produk. Dengan harga terjangkau dan REPUTASI yang TERPERCAYA, kami menyediakan jasa cek otentikasi produk anda.',
            'order' => 2,
        ],
        ]);
    }
}
