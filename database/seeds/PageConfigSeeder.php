<?php

use Illuminate\Database\Seeder;

class PageConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pageConfig = [
            [
                'email' => 'checkyuk.id@gmail.com',
                'address' => 'Bandung, Indonesia',
                'phone_number' => '0813 1961 2102',
                'instagram_account' => 'https://instagram.com/checkyuk',
                'whatsapp_api' => 'https://wa.me/628139612102?text=Bro%20LC%20dong',
                'aboutus_caption1' => 'CHECKYUK - ABOUT US',
                'aboutus_caption2' => 'YOUR DAILY LEGITCHECKER!',
                'aboutus_caption3' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!',
                'aboutus_background_image' => 'uploads/home-background.jpg',
                'aboutus_image' => 'uploads/page-images/about-us-image.jpg',
                'authentication_caption1' => 'CARI TAHU KEASLIAN PRODUK ANDA',
                'authentication_caption2' => 'Malu dong kalau punya produk mahal tapi dikira palsu? Yuk, kita bantu tentuin keaslian produk lo!!',
                'authentication_image_why_checkyuk' => 'uploads/checkyuk-1-xd.jpg',
                'footer_caption' => 'Checkyuk adalah platform untuk para pecinta HYPE yang SANGAT MENGHARGAI authenticity (keaslian) suatu produk.',
                'history_caption1' => 'CARI TAHU KEASLIAN PRODUK ANDA!', 
                'history_caption2' => 'Malu dong kalau punya produk mahal tapi dikira palsu? Yuk, kita bantu tentuin keaslian produk lo!!', 
                'mylchistory_caption1' => 'History Legit Check', 
                'mylchistory_caption2' => 'Disini, bro and sis bisa liat history LC',
                'mylchistory_caption3' => 'HISTORY LEGIT CHECK',
                'photos_guide_caption1' => 'CHECKYUK - GUIDE PHOTOS', 
                'photos_guide_caption2' => 'YOUR DAILY LEGITCHECKER!',
                'photos_guide_caption3' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!', 
                'photos_guide_background_image' => 'uploads/home-background.jpg',
                'photos_guide_title' => 'PHOTO GUIDES',
                'refund_caption1' => 'CHECKYUK - REFUND',
                'refund_caption2' => 'YOUR DAILY LEGITCHECKER!',
                'refund_caption3' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!',
                'refund_background_image' => 'uploads/home-background.jpg',
                'refund_title' => 'CHECKYUK REFUND POLICY',
                'termscondition_caption1' => 'CHECKYUK TERMS AND CONDITION',
                'termscondition_caption2' => 'YOUR DAILY LEGITCHECKER!',
                'termscondition_caption3' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!', 
                'termscondition_background_image' => 'uploads/home-background.jpg',
                'termscondition_title' => 'Terms and Condition', 
                'welcome_caption1' => 'YOUR DAILY LEGITCHECKER!',
                'welcome_caption2' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!',
                'welcome_background_image' => 'uploads/home-background.jpg', 
                'whychooseus_title' => 'Why choose us',
                'whychooseus_caption' => 'Checkyuk adalah platform untuk para pecinta HYPE yang SANGAT MENGHARGAI authenticity (keaslian) suatu produk. Dengan harga terjangkau dan REPUTASI yang TERPERCAYA, kami menyediakan jasa cek otentikasi produk anda.',
                'whychooseus_background_image' => 'uploads/chooseus/chooseus-bg.jpg'
            ],
        ];

        DB::table('page_config')->insert($pageConfig);
    }
}
