<?php

use Illuminate\Database\Seeder;

class TermsAndConditionSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms_and_condition_settings')->insert([
            [
                'caption' => '<p><strong>CHECKYUK</strong></p>

                <p><strong>1.</strong>&nbsp;Checkyuk merupakan suatu lembaga yang bergerak dalam bidang Legit Checker, dimana pihak Checkyuk bisa mengiindentikasi apakah barang yang anda miliki adalah Asli apa Palsu. Checkyuk memiliki tim yang sudah berpengalaman dalam dunia sneakers selama kurun waktu 4 tahun jadi tim checkyuk akan menggunakan pengalaman yang mereka miliki dalam mengecek Barang anda.</p>
                
                <p>&nbsp;</p>
                
                <p><strong>2. PROSES PENGECEKAN</strong></p>
                
                <ul>
                    <li>
                    <p>Pihak pengecek akan memberikan foto-foto yang jelas dan detail kepada tim CHECKYUK foto dapat dilihat pada <a href="checkyuk.com/photos-guide-page">Photos Guide</a></p>
                    </li>
                    <li>
                    <p>CHECKYUK mempunyai kewajiban untuk meminta foto ulang. Jika foto yang diberikan pihak pengecek kurang jelas</p>
                    </li>
                    <li>
                    <p>Pihak pengecek setelah mengirimkan foto kepada tim CHECKYUK, pihak pengecek harus membayar/ mentransfer kepada&nbsp; pihak CHECKYUK dengan nominal dan rekening yang tercantum pada pembayaran</p>
                    </li>
                    <li>
                    <p>Pihak mengecek bersedia mengikuti Terms and Condition dari Pihak CHECKYUK jika sudah melakukan Pembayaran</p>
                    </li>
                    <li>
                    <p>Pihak mengecek bersedia mengikuti Terms and Condition dari Pihak CHECKYUK jika sudah melakukan Pembayaran</p>
                    </li>
                </ul>
                
                <p>&nbsp;</p>
                
                <p><strong>3. WAKTU PENGECEKAN</strong></p>
                
                <ul>
                    <li>
                    <p>Tim CHECKYUK mengecek barang yang diberikan oleh pengecek selama Minimal 1 Jam</p>
                    </li>
                    <li>
                    <p>Tim CHECKYUK akan memberikan Informasi jika Sudah selesai dalam mengecek melalui Email Pengecek</p>
                    </li>
                </ul>
                
                <p>&nbsp;</p>
                
                <p><strong>&nbsp;4. HASIL PENGECEKAN</strong></p>
                
                <ul>
                    <li>
                    <p>Hasil yang diberikan merupakan hasil yang MUTLAK yang diberikan oleh TIM CHECKYUK</p>
                    </li>
                    <li>
                    <p>Hasil yang diberikan oleh tim CHECKYUK merupakan Opini dari Tim CHECKYUK</p>
                    </li>
                    <li>
                    <p>Hasil yang diberikan oleh tim CHECKYUK akan berada pada LC History, untuk menghimbau adanya Manipulasi Jawaban/ hasil</p>
                    </li>
                </ul>
                
                <p>&nbsp;</p>
                
                <p><strong>5. PENERBITAN HASIL</strong></p>
                
                <ul>
                    <li>
                    <p>Hasil yang dapat diterbitkan berupa :</p>
                
                    <ul>
                        <li>
                        <p>AUTHENTIC :&nbsp;Menandakan item asli.</p>
                        </li>
                        <li>
                        <p>FAKE : Menandakan item palsu.</p>
                        </li>
                        <li>
                        <p>INDEFINEABLE : Menandakan bahwa tidak bisa memberikan hasil, maka fee legit check akan di refund full.</p>
                        </li>
                    </ul>
                    </li>
                    <li>
                    <p>Jika pihak pengecek berpendapat bahwa hasil yang diterbitkan salah, maka pihak pengecek dapat menghubungi CHECKYUK di alamat yang sudah tertera. Pihak CHECKYUK bersedia menggantikan sebesar 50% dari harga item sesuai kondisi dan harga pasaran Indonesia.</p>
                    </li>
                    <li>
                    <p>Untuk proses komplain jika pihak pengecek berpendapat hasil yang diterbitkan tidak sesuai, maka pengecek memiliki hak untuk komplain dalam batas waktu 4 (EMPAT) JAM dari penerbitan hasil CHECKYUK. Prosedur komplain adalah:</p>
                
                    <ul>
                        <li>
                        <p>Penyertaan bukti hasil pengecekan dari 2 (DUA) LEMBAGA LEGITCHECKER berbasis di INDONESIA dan berbadan hukum, disertai dengan penjelasan-penjelasan.</p>
                        </li>
                        <li>
                        <p>Foto - foto yang disediakan ke LEMBAGA LEGITCHEKCER lain haruslah foto-foto yang sama yang diberi ke team CHECKYUK.</p>
                        </li>
                    </ul>
                    </li>
                </ul>
                
                <p>Syarat dan ketentuan efektif sejak tanggal 24 April 2020 dan dapat berubah sewaktu-waktu tanpa pemberitahuan.</p>',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
