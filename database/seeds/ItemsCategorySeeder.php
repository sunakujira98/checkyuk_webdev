<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $itemsCategory = [
            ['title' => 'General Release'],
            ['title' => 'Rare Release'],
            ['title' => 'Luxury Leather Goods']
        ];

        DB::table('items_category')->insert($itemsCategory);
    }
}
