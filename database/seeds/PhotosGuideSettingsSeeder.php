<?php

use Illuminate\Database\Seeder;

class PhotosGuideSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('guide_photos_settings')->insert([
            [
                'title' => 'Shoe In General',
                'image_url' => 'uploads/photo-guides/shoesgeneral_watermark.jpg',
                'order' => 1,
                'caption' => '<p>1. Foto sepatu segala sisi<br />
                2. Ousole sepatu<br />
                3. Belakang kedua sepatu dan closeup heel logo tiap sepatu<br />
                4. Tongue tag luar dan dalam<br />
                5. Insole kedua sepatu<br />
                6. Size tag sepatu<br />
                7. Box segala sisi</p>'
            ],
            [
                'title' => 'Supreme Bag',
                'image_url' => 'uploads/photo-guides/supremebag_watermark.jpg',
                'order' => 2,
                'caption' => '<p>1. FOTO TAS SEGALA SISI<br />
                2. Foto TAG merah di samping<br />
                3. Cordura / DP tag depan belakang<br />
                4. Foto dalam tas<br />
                5. Tag didalam Tas<br />
                6. Engraving di kunci/clasp tali ba</p>'
            ],
            [
                'title' => 'Wallet',
                'image_url' => 'uploads/photo-guides/wallet_watermark.jpg',
                'order' => 3,
                'caption' => '<p>1. Wallet segala sisi (foto jauh luar dalam)<br />
                2. Brand stamp<br />
                3. White tag / datecode / tag di sela sela wallet<br />
                4. Box segala sisi<br />
                5. Kelengkapan (dustbag, receipt, warranty card</p>'
            ],            [
                'title' => 'Tee/Hoodie',
                'image_url' => 'uploads/photo-guides/tee_watermark.jpg',
                'order' => 2,
                'caption' => '<p>1. Foto jauh print depan belakang<br />
                2. Neck tag closeup<br />
                3. Size tag<br />
                4. Wash tag (jika 3 lembar atau lebih, foto semua jelas dan sejajar ke kamera)<br />
                5. Kelengkapan (receipt, bag, plastic, etc.)</p>'
            ],            [
                'title' => 'Yeezy',
                'image_url' => 'uploads/photo-guides/yeezy_watermark.jpg',
                'order' => 2,
                'caption' => '<p>1. FOTO SEPATU kiri kanan<br />
                2. FOTO SIZE tag kiri kanan<br />
                3. FOTO INSOLE kiri kanan<br />
                4. FOTO BOOST<br />
                5. FOTO JAHITAN DALAM SEPATU<br />
                6. FOTO BOX SEGALA SISI DAN LABEL BO</p>'
            ],
        ]);
    }
}
