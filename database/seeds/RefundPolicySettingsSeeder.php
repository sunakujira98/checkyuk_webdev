<?php

use Illuminate\Database\Seeder;

class RefundPolicySettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('refund_policy_settings')->insert([
            [
                'caption' => 'CHECKYUK dapat memutuskan hasil Legit Check dengan status "INDEFINEABLE" yang artinya tidak bisa ditentukan oleh team kami, karena berbagai macam hal seperti aturan foto yang tidak mengikuti guideline kami. Ini disebabkan untuk menjaga hasil otentikasi yang dikeluarkan oleh pihak CHECKYUK agar tetap AKURAT dan TERPERCAYA. Maka dengan itu CHECKYUK akan mengembalikan akan melakukan refund dalam 3 x 24jam dari hasil otentikasi yang dikirim via email.',
                'order' => 1,
            ],
            [
                'caption' => 'Simply email us under our tnc, dengan screenshot hasil/email dari chekcyuk dan isi bank details kalian Nama bank, No rek, A.n. ,  Bukti cek dari cekyuk, Bukti dan penjelasan tim lain/ indefine :',
                'order' => 2,
            ]
        ]);
    }
}
