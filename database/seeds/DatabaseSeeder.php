<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ItemsCategorySeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(PageConfigSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(AboutUsSettingsSeeder::class);
        $this->call(RefundPolicySettingsSeeder::class);
        $this->call(WhyChooseUsSettingsSeeder::class);
        $this->call(TermsAndConditionSettingsSeeder::class);
        $this->call(PhotosGuideSettingsSeeder::class);
        // $this->call(HomeSettingsSettingsSeeder::class);
    }
}
