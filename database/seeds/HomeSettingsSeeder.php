<?php

use Illuminate\Database\Seeder;

class HomeSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('home_settings')->insert([
            [
                'caption' => 'YOUR DAILY LEGITCHECKER!',
                'order' => 1,
            ],
            [
                'caption' => 'LEGIT CHECKER TERPERCAYA DAN TERCEPAT SE-INDONESIA!',
                'order' => 2,
            ],
        ]);
    }
}
