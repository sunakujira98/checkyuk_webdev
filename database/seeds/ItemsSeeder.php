<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $items = [
            [
                'title' => 'Nike',
                'abbreviation' => 'NIKE', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/nike-logo.png',
                'price' => 50000
            ],
            [
                'title' => 'Adidas',
                'abbreviation' => 'ADIDAS', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/adidas-logo.png',
                'price' => 50000
            ],
            [
                'title' => 'Vans',
                'abbreviation' => 'VANS', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/vans-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Bape',
                'abbreviation' => 'BAPE', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/bape-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Marcelo',
                'abbreviation' => 'MARCELO', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/marcelo-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Offwhite',
                'abbreviation' => 'OW', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/offwhite-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Undefeated',
                'abbreviation' => 'UNDEFEATED', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/undefeated-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Coach',
                'abbreviation' => 'COACH', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/coach-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Kenzo',
                'abbreviation' => 'KENZO', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/kenzo-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'MCM',
                'abbreviation' => 'MCM', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/mcm-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Palace',
                'abbreviation' => 'PALACE', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/palace-jpg.jpg', 
                'price' => 50000
            ],
            [
                'title' => 'Supreme',
                'abbreviation' => 'SUPREME', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/supreme-logo.jpg', 
                'price' => 50000
            ],
            [
                'title' => 'Jordan',
                'abbreviation' => 'JORDAN', 
                'items_category_id' => 1, 
                'image_url' => 'uploads/brand-logo/jordan-logo.png', 
                'price' => 50000
            ],
            [
                'title' => 'Balenciaga',
                'abbreviation' => 'BALENCIAGA', 
                'items_category_id' => 2, 
                'image_url' => 'uploads/brand-logo/balenciaga-logo.png', 
                'price' => 85000
            ],
            [
                'title' => 'Givency Shoes',
                'abbreviation' => 'GS', 
                'items_category_id' => 2, 
                'image_url' => 'uploads/brand-logo/givency-logo.jpg', 
                'price' => 85000
            ],
            [
                'title' => 'Alexander Mcqueen',
                'abbreviation' => 'AM', 
                'items_category_id' => 2, 
                'image_url' => 'uploads/brand-logo/Alexander-McQueen-logo.png', 
                'price' => 85000
            ],
            [
                'title' => 'Gucci',
                'abbreviation' => 'GUCCI', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/gucci-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'GIVENCY',
                'abbreviation' => 'GIVENCY', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/givency-logo.jpg', 
                'price' => 100000
            ],
            [
                'title' => 'Louis Vuitton',
                'abbreviation' => 'LV', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/louis-vuitton-2-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Goyard',
                'abbreviation' => 'GOYARD', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/goyard-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Hermes',
                'abbreviation' => 'HERMES', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/hermes-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Prada',
                'abbreviation' => 'PRADA', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/prada-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Channel',
                'abbreviation' => 'CHANNEL', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/channel-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Fendi',
                'abbreviation' => 'FENDI', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/fendi-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Tory Burch',
                'abbreviation' => 'TB', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/tory-burch-logo.png', 
                'price' => 100000
            ],
            [
                'title' => 'Salvatore',
                'abbreviation' => 'SVT', 
                'items_category_id' => 3, 
                'image_url' => 'uploads/brand-logo/salvatore-logo.png', 
                'price' => 100000
            ]

        ];

        DB::table('items')->insert($items);
    }
}
