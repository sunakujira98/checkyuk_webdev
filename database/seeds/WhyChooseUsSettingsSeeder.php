<?php

use Illuminate\Database\Seeder;

class WhyChooseUsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('why_choose_us_settings')->insert([
            [
                'judul' => 'Team Berpengalaman',
                'caption' => 'Berpengalaman lebih dari 5 tahun di dunia Hype',
                'image_url' => 'uploads/chooseus/experience.png',
                'order' => 1,
            ],
            [
                'judul' => 'Harga terjangkau',
                'caption' => 'Mulai dari 50RIBU. Kita bisa menjamin keaslian produk kalian.',
                'image_url' => 'uploads/chooseus/offer.png',
                'order' => 2,
            ],
            [
                'judul' => 'Proses Cepat',
                'caption' => 'Hanya dalam 30menit!',
                'image_url' => 'uploads/chooseus/time.png',
                'order' => 3,
            ],
        ]);
    }
}
