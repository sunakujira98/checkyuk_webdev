<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admins')->insert([
        [
            'email' => 'checkyuk.id@gmail.com',
            'name' => 'Checkyuk Admin',
            'password' => bcrypt("checkyuk123"),
        ]
        ]);
    }
}
