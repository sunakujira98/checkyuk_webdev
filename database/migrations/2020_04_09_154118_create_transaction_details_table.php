<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->string('id_legit_check');
            $table->string('product_name');
            $table->text('comment')->nullable();
            $table->text('comment_from_admin')->nullable();
            $table->boolean('is_original')->nullable();
            $table->unsignedBigInteger('transaction_id');
            $table->foreign('transaction_id')->references('id')->on('transaction');
            $table->unsignedBigInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items');
            $table->integer('qty');
            $table->boolean('images_is_not_clear')->nullable();
            $table->text('comment_images_is_not_clear')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
