<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_config', function (Blueprint $table) {
            $table->id();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('instagram_account')->nullable();
            $table->string('whatsapp_api')->nullable();
            $table->string('aboutus_caption1')->nullable();
            $table->string('aboutus_caption2')->nullable();
            $table->string('aboutus_caption3')->nullable();
            $table->string('aboutus_background_image')->nullable();
            $table->string('aboutus_image')->nullable();
            $table->string('authentication_caption1')->nullable();
            $table->string('authentication_caption2')->nullable();
            $table->string('authentication_image_why_checkyuk')->nullable();
            $table->string('footer_caption')->nullable();
            $table->string('history_caption1')->nullable();
            $table->string('history_caption2')->nullable();
            $table->string('mylchistory_caption1')->nullable();
            $table->string('mylchistory_caption2')->nullable();
            $table->string('mylchistory_caption3')->nullable();
            $table->string('photos_guide_caption1')->nullable();
            $table->string('photos_guide_caption2')->nullable();
            $table->string('photos_guide_caption3')->nullable();
            $table->string('photos_guide_background_image')->nullable();
            $table->string('photos_guide_title')->nullable();
            $table->string('refund_caption1')->nullable();
            $table->string('refund_caption2')->nullable();
            $table->string('refund_caption3')->nullable();
            $table->string('refund_background_image')->nullable();
            $table->string('refund_title')->nullable();
            $table->string('termscondition_caption1')->nullable();
            $table->string('termscondition_caption2')->nullable();
            $table->string('termscondition_caption3')->nullable();
            $table->string('termscondition_background_image')->nullable();
            $table->string('termscondition_title')->nullable();
            $table->string('welcome_caption1')->nullable();
            $table->string('welcome_caption2')->nullable();
            $table->string('welcome_background_image')->nullable();
            $table->string('whychooseus_title')->nullable();
            $table->string('whychooseus_caption')->nullable();
            $table->string('whychooseus_background_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_config');
    }
}
