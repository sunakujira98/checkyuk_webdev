<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
	Route::crud('item', 'ItemCrudController');
	Route::crud('item-category', 'ItemCategoryCrudController');
	Route::crud('transaction', 'TransactionCrudController');
	Route::crud('transactiondetail', 'TransactionDetailCrudController');
	Route::crud('transactiondetailimage', 'TransactionDetailImageCrudController');
    Route::crud('pageconfig', 'PageConfigCrudController');
    Route::crud('aboutussettings', 'AboutUsSettingsCrudController');
    Route::crud('homesetting', 'HomeSettingCrudController');
    Route::crud('servicesetting', 'ServiceSettingCrudController');
    Route::crud('whychooseussetting', 'WhyChooseUsSettingCrudController');
    Route::crud('footersetting', 'FooterSettingCrudController');
    Route::get('transactiondetail/{id}/emailCustomer', 'TransactionDetailCrudController@emailCustomer');
    Route::get('transactiondetail/{id}/change-status/{status}', 'TransactionDetailCrudController@changeStatus');
    Route::get('transactiondetail/{id}/add-comments', 'TransactionDetailCrudController@addComments');
    Route::get('transactiondetail/{id}/foto-tidakjelas/{status}', 'TransactionDetailCrudController@fotoTidakJelas');
    Route::get('transaction/{id}/emailCustomerVerifyPayment', 'TransactionCrudController@emailCustomerVerifyPayment');
    Route::get('transaction/{id}/verify-payment/{status}', 'TransactionCrudController@changeVerifyPayment');
    // Backpack\NewsCRUD
    Route::crud('article', 'ArticleCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('refundpolicysettings', 'RefundPolicySettingsCrudController');
    Route::crud('termsconditionpolicysettings', 'TermsConditionPolicySettingsCrudController');
    Route::crud('guidephotossettings', 'GuidePhotosSettingsCrudController');
}); // this should be the absolute last line of this file