<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@homePageAfterLogin')->name('home')->middleware(['auth', 'verified']);
Route::get('/', "HomeController@homePage");

Route::get('/article-page',[
    'uses' => 'HomeController@articlePage'
]);
Route::get('/article-page-details/{slug}',[
    'uses' => 'HomeController@articlePageDetail',
    'as' => 'blog.details'
]);
Route::get('/about-us-page',[
    'uses' => 'HomeController@aboutUsPage'
]);
Route::get('/refund-policy-page',[
    'uses' => 'HomeController@refundPolicyPage'
]);
Route::get('/terms-condition-page',[
    'uses' => 'HomeController@termsAndConditionPolicyPage'
]);
Route::get('/photos-guide-page', [
    'uses' => 'HomeController@photosGuidePage'
]);

Route::get('/legit-check-history', "HomeController@authenticationHistoryPage");
Route::get('/legit-checkk-history/{id}/{name}-authentication-history', [
    'uses' => 'HomeController@authenticationHistoryBrandPage',
    'as' => 'history.Result'
]);
Route::get('/legit-check-history-detail/{id}/{name}-legit-check', [
    'uses' => 'HomeController@authenticationHistoryBrandPage',
    'as' => 'history.Result'
]);
Route::get('/legit-check-history-detail-view/{id}/{name}-legit-check', [
    'uses' => 'HomeController@authenticationHistoryViewPage',
    'as' => 'history.ResultDetailView'
]);

Route::get('/authentication-service', "HomeController@authenticationPage");
Route::get('/authentication-service-{name}', [
    'uses' => 'HomeController@authenticationAddToCartPage',
    'as' => 'authenticate.addToCart'
]);
Route::post('/search-lc', [
    'uses' => 'HomeController@authenticationPageSearch',
    'as' => 'search.lc'
]);

Route::get('/checkout', "HomeController@billingPage")->middleware('auth');
Route::get('/lc-history', "HomeController@LCHistoryPage")->middleware('auth');
Route::get('/lc-history-view/{id}', [
    'uses' => 'HomeController@LCHistoryDetailPage',
    'as' => 'lc-history.viewDetail'
])->middleware('auth');;
Route::get('/search-brand', [
    'uses' => 'HomeController@authenticationPageSearch',
    'as' => 'search.brand'
]);

Route::post('/checkout/buat-pembayaran', [
    'uses' => 'TransactionController@placeOrder',
    'as' => 'transaction.placeOrder'
]);

Route::post('/upload-missing-images/{id}', [
    'uses' => 'UploadController@uploadMissingImages',
    'as' => 'upload.missingImages'
]);

Route::get('/add-to-cart/{id}', [
    'uses' => 'ItemsController@getAddToCart',
    'as' => 'items.addToCart'
]);
Route::get('/remove-cart/{id}', [
    'uses' => 'ItemsController@removeItemFromCart',
    'as' => 'items.removeFromCart'
]);
