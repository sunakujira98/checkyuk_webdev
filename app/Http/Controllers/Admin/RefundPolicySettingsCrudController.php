<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RefundPolicySettingsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RefundPolicySettingsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RefundPolicySettingsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\RefundPolicySettings');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/refundpolicysettings');
        $this->crud->setEntityNameStrings('Refund Policy', 'Refund Policy');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'order',
                'label' => 'Urutan',
            ], [
                'name' => 'caption',
                'label' => 'caption',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        {
            $this->crud->addFields([
                [
                    'name' => 'order',
                    'label' => 'Urutan',
                ], [
                    'name' => 'caption',
                    'label' => 'caption',
                ]
            ]);
        }
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
