<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WhyChooseUsSettingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class WhyChooseUsSettingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WhyChooseUsSettingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\WhyChooseUsSetting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/whychooseussetting');
        $this->crud->setEntityNameStrings('Why Choose Us', 'Why Choose Us');
    }

    protected function setupListOperation()
    { 
        $this->crud->addColumns([
            [
                'name' => 'judul',
                'label' => 'Judul',
            ],
            [
                'name' => 'caption', 
                'label' => 'Caption',
            ], [
                'name' => 'image_url',
                'type' => 'image',
                'label' => 'Image',
            ], [
                'name' => 'order',
                'label' => 'Order',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'judul',
                'label' => 'Judul',
            ],
            [
                'name' => 'caption', 
                'label' => 'Caption',
            ],[
                'name' => 'order', 
                'label' => 'Order',
            ],[
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'browse',
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}