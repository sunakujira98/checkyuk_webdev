<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\TransactionDetailRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\TransactionDetails;
use App\Models\User;
use App\Notifications\EmailTransactionStatus;
use App\Notifications\EmailPhotosIsNotClear;
use Illuminate\Http\Request;
/**
 * Class TransactionDetailCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionDetailCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\TransactionDetails');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transactiondetail');
        $this->crud->setEntityNameStrings('Detail', 'Details');
        $this->crud->addButtonFromView('line', 'emailCustomer', 'send_email_customer');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID Detail Transaksi',
            ], 
            [
                'name' => 'transaction_id',
                'label' => 'ID Transaksi',
                'type' => 'select',
                'entity' => 'transaction',
                'attribute' => 'id',
                'model' => 'App\Models\Transaction'
            ],
            [
                'name' => 'created_at',
                'label' => 'Tanggal',
            ],
            [
                'name' => 'items_id',
                'label' => 'Brand',
                'type' => 'select',
                'entity' => 'items',
                'attribute' => 'title',
                'model' => 'App\Models\Items',
            ],
            [
                'name' => 'product_name',
                'label' => 'Product Name'
            ], [
                'name' => 'is_original',
                'label' => 'Authentic?',
                'type' => 'select_from_array',
                'options' => [NULL => 'BELUM DI CEK', 0 => 'FAKE' , 1 => 'ORIGINAL', 2 => 'INDEFINEABLE'],
                'allows_null' => true,
            ], [
                'name' => 'transaction.user.name',
                'label' => 'Customer Name',
            ],
            [
                'name' => 'transaction_details_images',
                'label' => 'Item Image',
                'type' => 'select_multiple',
                'attribute' => 'image_url',
                'entity' => 'transaction_details_images',
                'model' => 'App\Models\TransactionDetailsImages'
            ], [
                'name'=> 'qty',
                'label'=> 'Quantity'
            ]
        ]);
        $this->crud->enableExportButtons();
    }

    protected function setupCreateOperation()
    {
        // $this->crud->setValidation(TransactionDetailRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'product_name',
                'label' => 'Product Name'
            ], [
                'name' => 'is_original',
                'label' => 'Authentic?',
                'type' => 'select_from_array',
                'options' => [NULL => 'BELUM DI CEK', 0 => 'FAKE' , 1 => 'ORIGINAL', 2 => 'INDEFINEABLE'],
                'allows_null' => true,
            ], [
                'name' => 'transaction_id',
                'label' => 'Transaction',
                'type' => 'select',
                'entity' => 'transaction',
                'attribute' => 'id',
                'model' => 'App\Models\Transaction'
            ], [
                'name'=> 'qty',
                'label'=> 'Quantity'
            ]
        ]);
    }

    public function emailCustomer($id){
        $transactionDetail = TransactionDetails::find($id);
        if($transactionDetail->is_original){
            $original = 'ORIGINAL';
        }else{
            $original = 'FAKE';
        }
        $customer_id = $transactionDetail->transaction->users_id;
        $customer = User::find($customer_id);
        $customer->notify(new EmailTransactionStatus($original,$transactionDetail));
        
        return redirect()->back();
    }

    public function changeStatus($id, $status){
        $transactionDetail = TransactionDetails::find($id);
        if ($status == "2") {
            $transactionDetail->is_original = (int) $status;
        } else {
            $transactionDetail->is_original = $status;
        }
        $transactionDetail->save();

        $transactionDetail = TransactionDetails::find($id);

        if($transactionDetail->is_original == 2){
            $original = 'INDEFINEABLE';
        } else {
            if($transactionDetail->is_original){
                $original = 'AUTHENTIC';
            }else{
                $original = 'FAKE';
            }
        }

        $customer_id = $transactionDetail->transaction->users_id;
        $customer = User::find($customer_id);
        $customer->notify(new EmailTransactionStatus($original, $transactionDetail));
        
        return redirect()->back()->with(['success' => '<strong>Sukses!</strong> Berhasil melakukan otentikasi.']);
    }

    public function addComments($id, Request $request) {
        $transactionDetail = TransactionDetails::find($id);
        $transactionDetail->comment_from_admin = $request->get('komentar');
        $transactionDetail->save();

        return redirect()->back()->with(['success' => '<strong>Sukses!</strong> Sukses menambah komentar.']);
    }

    public function fotoTidakJelas($id, $status, Request $request) {
        $transactionDetail = TransactionDetails::find($id);
        $transactionDetail->images_is_not_clear = $status;
        $transactionDetail->comment_images_is_not_clear = $request->get('keteranganfoto');
        $transactionDetail->save();
        $status = "FOTO KURANG JELAS";

        $customer_id = $transactionDetail->transaction->users_id;
        $customer = User::find($customer_id);
        $customer->notify(new EmailPhotosIsNotClear($status, $transactionDetail));
        
        return redirect()->back()->with(['success' => '<strong>Sukses!</strong> Sukses mengirim email kepada customer untuk mengupload gambar ulang.']);
    }
}
