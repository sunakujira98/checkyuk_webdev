<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\TransactionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Transaction;
use App\Models\User;
use App\Notifications\EmailTransactionPaymentVerification;

/**
 * Class TransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Transaction');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transaction');
        $this->crud->setEntityNameStrings('Transaction', 'Transactions');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID Transaksi',
            ], [
                'name' => 'created_at',
                'label' => 'Tanggal Transaksi',
            ], [
                'name' => 'users_id',
                'label' => 'Nama User',
                'type' => 'select',
                'entity' => 'user',
                'attribute' => 'name',
                'model' => 'App\Models\User'
            ], [
                'name' => 'user.email',
                'label' => 'Email User',
                'type' => 'select',
                'entity' => 'user',
                'attribute' => 'email',
                'model' => 'App\Models\User'
            ], [
                'name' => 'grand_total',
                'label' => 'Grand Total',
            ], [
                'name' => 'payment_proof_image',
                'label' => 'Image',
                'type' =>'image',
            ]
        ]);
        $this->crud->enableExportButtons();
    }

    protected function setupCreateOperation()
    {
        // $this->crud->setValidation(TransactionRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->addFields([
            [
                'name' => 'users_id',
                'label' => 'Nama User',
                'type' => 'select2',
                'entity' => 'user',
                'attribute' => 'name',
                'model' => 'App\Models\User'
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    

    public function emailCustomer($id){

    }

    public function changeVerifyPayment($id, $status){
        $transaction = Transaction::find($id);
        $transaction->payment_is_verified = $status;
        $transaction->save();

        if($transaction->payment_is_verified){
            $payment_is_verified = 'BERHASIL DIVERIFIKASI';
        }else{
            $payment_is_verified = 'GAGAL DIVERIFIKASI';
        }
        $customer_id = $transaction->users_id;
        $customer = User::find($customer_id);
        $namaUser = User::select('name')->where('id', '=', $customer_id)->first();
        $customer->notify(new EmailTransactionPaymentVerification($status, $namaUser));
        
        if ($transaction->payment_is_verified) {
            return redirect()->back()->with(['success' => '<strong>Sukses!</strong> Pembayaran telah diverifikasi']);
        } else {
            return redirect()->back()->with(['info' => 'Pembayaran telah digagalkan.']);
        }
    }
}
