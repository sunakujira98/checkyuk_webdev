<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\ItemCategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ItemCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ItemCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\ItemsCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/item-category');
        $this->crud->setEntityNameStrings('Category', 'Categories');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'title', 
                'label' => 'Title',
            ],[
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'image',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
         $this->crud->addFields([
             [
                'name' => 'title', 
                'label' => 'Title',
            ],[
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'browse',
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
