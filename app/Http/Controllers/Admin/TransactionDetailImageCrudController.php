<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\TransactionDetailImageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransactionDetailImageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionDetailImageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\TransactionDetailsImages');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transactiondetailimage');
        $this->crud->setEntityNameStrings('Detail Image', 'Detail Images');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'transaction_details_id',
                'label' => 'ID Detail Transaksi'
            ], [
                'name' => 'image_url',
                'label' => 'Image',
                'type' => 'image',
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        // $this->crud->setValidation(TransactionDetailImageRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
