<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageConfigRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageConfigCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageConfigCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PageConfig');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pageconfig');
        $this->crud->setEntityNameStrings('Page Config', 'Page Configs');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'email', 
                'label' => 'Email',
            ], [
                'name' => 'address',
                'label' => 'Alamat',
            ], [
                'name' => 'phone_number',
                'label' => 'phone_number',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'whatsapp_api',
                'label' => 'Whatsapp URL API',
            ], [
                'name' => 'aboutus_caption1',
                'label' => 'About US Caption 1',
            ], [
                'name' => 'aboutus_caption2',
                'label' => 'About US Caption 2',
            ], [
                'name' => 'aboutus_caption3',
                'label' => 'About US Caption 3',
            ], [
                'name' => 'aboutus_background_image',
                'label' => 'About US Background Image',
                'type' => 'image',
            ], [
                'name' => 'aboutus_image',
                'label' => 'About US Image',
                'type' => 'image',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'email', 
                'label' => 'Email',
            ], [
                'name' => 'address',
                'label' => 'Alamat',
            ], [
                'name' => 'phone_number',
                'label' => 'phone_number',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'whatsapp_api',
                'label' => 'Whatsapp URL API',
            ], [
                'name' => 'aboutus_caption1',
                'label' => 'About US Caption 1',
            ], [
                'name' => 'aboutus_caption2',
                'label' => 'About US Caption 2',
            ], [
                'name' => 'aboutus_caption3',
                'label' => 'About US Caption 3',
            ], [
                'name' => 'aboutus_background_image',
                'label' => 'About US Background Image',
                'type' => 'browse',
            ], [
                'name' => 'aboutus_image',
                'label' => 'About US Image',
                'type' => 'browse',
            ], [
                'name' => 'authentication_caption1',
                'label' => 'Jasa Otentikasi Caption 1',
            ], [
                'name' => 'authentication_caption2',
                'label' => 'Jasa Otentikasi Caption 2',
            ], [
                'name' => 'authentication_image_why_checkyuk',
                'label' => 'Jasa Otentikasi Gambar Bawah',
                'type' => 'browse',
            ], [
                'name' => 'footer_caption',
                'label' => 'Footer Caption',
            ], [
                'name' => 'history_caption1',
                'label' => 'History Caption 1',
            ], [
                'name' => 'history_caption2',
                'label' => 'History Caption 2',
            ], [
                'name' => 'mylchistory_caption1',
                'label' => 'MY LC History Caption 1',
            ], [
                'name' => 'mylchistory_caption2',
                'label' => 'MY LC History Caption 2',
            ], [
                'name' => 'mylchistory_caption3',
                'label' => 'MY LC History Caption 3',
            ], [
                'name' => 'photos_guide_caption1',
                'label' => 'Photos Guide Caption 1',
            ], [
                'name' => 'photos_guide_caption2',
                'label' => 'Photos Guide Caption 2',
            ], [
                'name' => 'photos_guide_caption3',
                'label' => 'Photos Guide Caption 3',
            ], [
                'name' => 'photos_guide_background_image',
                'label' => 'Photos Guide Background Image',
                'type' => 'browse',
            ], [
                'name' => 'photos_guide_title',
                'label' => 'Photos Guide Judul',
            ], [
                'name' => 'refund_caption1',
                'label' => 'Refund Caption 1',
            ], [
                'name' => 'refund_caption2',
                'label' => 'Refund Caption 2',
            ], [
                'name' => 'refund_caption3',
                'label' => 'Refund Caption 3',
            ], [
                'name' => 'refund_background_image',
                'label' => 'Refund Background Image',
                'type' => 'browse',
            ], [
                'name' => 'refund_title',
                'label' => 'Refund Judul',
            ], [
                'name' => 'termscondition_caption1',
                'label' => 'Terms & Condition Caption 1',
            ], [
                'name' => 'termscondition_caption2',
                'label' => 'Terms & Condition Caption 2',
            ], [
                'name' => 'termscondition_caption3',
                'label' => 'Terms & Condition Caption 3',
            ], [
                'name' => 'termscondition_background_image',
                'label' => 'Terms & Condition Background Image',
                'type' => 'browse',
            ], [
                'name' => 'termscondition_title',
                'label' => 'Terms & Condition Judul',
            ], [
                'name' => 'welcome_caption1',
                'label' => 'Home Caption 1',
            ], [
                'name' => 'welcome_caption2',
                'label' => 'Home Caption 2',
            ], [
                'name' => 'welcome_background_image',
                'label' => 'Home Background Image',
                'type' => 'browse',
            ], [
                'name' => 'whychooseus_title',
                'label' => 'Why Choose Us Judul',
            ], [
                'name' => 'whychooseus_caption',
                'label' => 'Why Choose Us Caption',
            ], [
                'name' => 'whychooseus_background_image',
                'label' => 'Why Choose Us Background Image',
                'type' => 'browse',
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'email', 
                'label' => 'Email',
            ], [
                'name' => 'address',
                'label' => 'Alamat',
            ], [
                'name' => 'phone_number',
                'label' => 'phone_number',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'instagram_account',
                'label' => 'IG Account',
            ], [
                'name' => 'whatsapp_api',
                'label' => 'Whatsapp URL API',
            ], [
                'name' => 'aboutus_caption1',
                'label' => 'About US Caption 1',
            ], [
                'name' => 'aboutus_caption2',
                'label' => 'About US Caption 2',
            ], [
                'name' => 'aboutus_caption3',
                'label' => 'About US Caption 3',
            ], [
                'name' => 'aboutus_background_image',
                'label' => 'About US Background Image',
                'type' => 'image',
            ], [
                'name' => 'aboutus_image',
                'label' => 'About US Image',
                'type' => 'image',
            ], [
                'name' => 'authentication_caption1',
                'label' => 'Jasa Otentikasi Caption 1',
            ], [
                'name' => 'authentication_caption2',
                'label' => 'Jasa Otentikasi Caption 2',
            ], [
                'name' => 'authentication_image_why_checkyuk',
                'label' => 'Jasa Otentikasi Gambar Bawah',
                'type' => 'image',
            ], [
                'name' => 'footer_caption',
                'label' => 'Footer Caption',
            ], [
                'name' => 'history_caption1',
                'label' => 'History Caption 1',
            ], [
                'name' => 'history_caption2',
                'label' => 'History Caption 2',
            ], [
                'name' => 'mylchistory_caption1',
                'label' => 'MY LC History Caption 1',
            ], [
                'name' => 'mylchistory_caption2',
                'label' => 'MY LC History Caption 2',
            ], [
                'name' => 'mylchistory_caption3',
                'label' => 'MY LC History Caption 3',
            ], [
                'name' => 'photos_guide_caption1',
                'label' => 'Photos Guide Caption 1',
            ], [
                'name' => 'photos_guide_caption2',
                'label' => 'Photos Guide Caption 2',
            ], [
                'name' => 'photos_guide_caption3',
                'label' => 'Photos Guide Caption 3',
            ], [
                'name' => 'photos_guide_background_image',
                'label' => 'Photos Guide Background Image',
                'type' => 'image',
            ], [
                'name' => 'photos_guide_title',
                'label' => 'Photos Guide Judul',
            ], [
                'name' => 'refund_caption1',
                'label' => 'Refund Caption 1',
            ], [
                'name' => 'refund_caption2',
                'label' => 'Refund Caption 2',
            ], [
                'name' => 'refund_caption3',
                'label' => 'Refund Caption 3',
            ], [
                'name' => 'refund_background_image',
                'label' => 'Refund Background Image',
                'type' => 'image',
            ], [
                'name' => 'refund_title',
                'label' => 'Refund Judul',
            ], [
                'name' => 'termscondition_caption1',
                'label' => 'Terms & Condition Caption 1',
            ], [
                'name' => 'termscondition_caption2',
                'label' => 'Terms & Condition Caption 2',
            ], [
                'name' => 'termscondition_caption3',
                'label' => 'Terms & Condition Caption 3',
            ], [
                'name' => 'termscondition_background_image',
                'label' => 'Terms & Condition Background Image',
                'type' => 'image',
            ], [
                'name' => 'termscondition_title',
                'label' => 'Terms & Condition Judul',
            ], [
                'name' => 'welcome_caption1',
                'label' => 'Home Caption 1',
            ], [
                'name' => 'welcome_caption2',
                'label' => 'Home Caption 2',
            ], [
                'name' => 'welcome_background_image',
                'label' => 'Home Background Image',
                'type' => 'image',
            ], [
                'name' => 'whychooseus_title',
                'label' => 'Why Choose Us Judul',
            ], [
                'name' => 'whychooseus_caption',
                'label' => 'Why Choose Us Caption',
            ], [
                'name' => 'whychooseus_background_image',
                'label' => 'Why Choose Us Background Image',
                'type' => 'image',
            ]
        ]);
    }
}
