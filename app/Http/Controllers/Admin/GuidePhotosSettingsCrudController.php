<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GuidePhotosSettingsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GuidePhotosSettingsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GuidePhotosSettingsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\GuidePhotosSettings');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/guidephotossettings');
        $this->crud->setEntityNameStrings('Guide Photos', 'Guide Photos');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'title', 
                'label' => 'Title',
            ], [
                'name' => 'caption',
                'label' => 'Caption',
            ], [
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'image',
            ], [
                'name' => 'order',
                'label' => 'Order',
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'title', 
                'label' => 'Title',
            ], [
                'name' => 'caption',
                'label' => 'Caption',
                'type' => 'ckeditor',
            ], [
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'browse',
            ], [
                'name' => 'order',
                'label' => 'Order',
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
