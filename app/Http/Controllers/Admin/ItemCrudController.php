<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\ItemRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class itemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ItemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Items');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/item');
        $this->crud->setEntityNameStrings('Item', 'Items');
    }

    protected function setupListOperation()
    { 
        $this->crud->addColumns([
            [
                'name' => 'title', 
                'label' => 'Title',
            ], [
                'name' => 'abbreviation',
                'label' => 'Abbreviation',
            ],
            [
                'name' => 'description',
                'label' => 'Description',
            ], [
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'image',
            ], [
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
            ], [
                'name' => 'items_category_id',
                'label' => 'Category',
                'type' => 'select',
                'entity' => 'items_category',
                'attribute' => 'title',
                'model' => 'App\Models\ItemsCategory'
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
             [
                'name' => 'title', 
                'label' => 'Title',
            ],[
                'name' => 'abbreviation',
                'label' => 'Abbreviation (Wajib isi buat nentuin kode LC)',
            ], [
                'name' => 'description',
                'label' => 'Description',
            ], [
                'name' => 'image_url',
                'label' => 'Image',
                'type' =>'browse',
            ], [
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
            ], [
                'name' => 'items_category_id',
                'label' => 'Category',
                'type' => 'select2',
                'entity' => 'items_category',
                'attribute' => 'title',
                'model' => 'App\Models\ItemsCategory'
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
