<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Models\Items;
use Session;
use Redirect;

class ItemsController extends Controller
{
    //

    public function getAddToCart(Request $request, $id)
    {
        $qty = $request->get('qty');
        $items = Items::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($items, $items->id, $qty);

        $request->session()->put('cart', $cart);
        // dd($request->session()->get('cart'));

        if (!Session::has('cart')) {
            return view('authentication.index', ['items' => $items]);
        }

        // $oldCart = Session::get('cart');
        // $cart = new Cart($oldCart);

        // return view('authentication.index', ['itemsSession' => $cart->items, 'totalPrice' => $cart->totalPrice]);

        return redirect()->back()->with(['info' => '<strong>Sukses!</strong> Berhasil menambah item ke dalam cart.', 'success_code', 5]);
    }

    
    public function removeItemFromCart(Request $request, $id)
    {
        // Get the product array
        $cart = Session::get('cart');

        $hargaRemove = 0;
        $qtyRemove = 0;

        $hargaRemove = $cart->items[$id]["price"];
        $qtyRemove = $cart->items[$id]["qty"];

        $cart->totalQty = $cart->totalQty - $qtyRemove;
        $cart->totalPrice = $cart->totalPrice - $hargaRemove;

        // Unset the first index (or provide an index)
        unset($cart->items[$id]); 

        // Overwrite the product session
        $request->session()->put('cart', $cart);

        if($cart->items == NULL) {
            Session::forget('cart');
        }
        
        //then you can redirect or whatever you need
        return redirect()->back()->with(['info' => '<strong>Sukses!</strong> Berhasil menghapus item dari cart.']);
    }
}
