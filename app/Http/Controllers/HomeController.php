<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Items;
use App\Models\ItemsCategory;
use App\Models\Transaction;
use App\Models\TransactionDetails;
use App\Models\AboutUsSettings;
use App\Models\RefundPolicySettings;
use App\Models\TermsConditionPolicySettings;
use App\Models\GuidePhotosSettings;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    // if($this->middleware('auth')){
    //     $this->middleware(['auth' => 'verified']);
    // }
    // }
    public function __construct()
    {
        // $this->middleware(['auth', 'verified'], );
        // $this->middleware(['auth', 'verified'], ['except' => [
        //     'homePage'
        // ]]);
    }

    private function getDataFromDb()
    {
        $data["page_config"] = \DB::table("page_config")->first();
        $data["why_choose_us"] = \DB::table("why_choose_us_settings")->get();

        return $data;
    }

    public function homePage()
    {
        $data = self::getDataFromDb();
        if (\Auth::check()) {
            return redirect('/home');
        }
        return view('welcome', $data);
    }

    public function homePageAfterLogin()
    {
        $data = self::getDataFromDb();
        return view('welcome', $data);
    }

    public function authenticationPage()
    {
        $data = self::getDataFromDb();
        $data['items'] = Items::orderBy('title', 'asc')->get();
        $data['itemsCategory'] = ItemsCategory::all();
        return view('authentication.index', $data);
    }

    public function authenticationAddToCartPage($name)
    {
        $data = self::getDataFromDb();
        $data['item'] = Items::where('title', '=', $name)->first();
        return view('authentication.authentication-add-to-cart', $data);
    }

    public function authenticationPageSearch(Request $request)
    {
        $q = $request->get('q');
        $data = self::getDataFromDb();
        $data['items'] = Items::where('title', 'LIKE', '%' . $q . '%')->orderBy('title', 'asc')->get();
        $data['search'] = "true";
        return view('authentication.index-search', $data);
    }

    public function authenticationHistoryPage()
    {
        $data = self::getDataFromDb();
        $data['items'] = Items::orderBy('title', 'asc')->get();

        return view('authentication.authentication-history', $data);
    }


    public function authenticationHistoryBrandPage($id)
    {
        $data = self::getDataFromDb();
        $data['items'] = Items::where('id', '=', $id)->first();
        $data['transactionDetails'] = TransactionDetails::where('items_id', '=', $id)->orderBy('created_at', 'desc')->whereNotNull('is_original')->paginate(12);

        return view('authentication.authentication-history-brand', $data);
    }

    public function authenticationHistoryViewPage($id)
    {
        $data = self::getDataFromDb();
        $data['items'] = Items::where('id', '=', $id)->first();
        $data['transactionDetails'] = TransactionDetails::where('id', '=', $id)->get();

        return view('authentication.authentication-history-brand-detail', $data);
    }

    public function billingPage()
    {
        $data = self::getDataFromDb();

        return view('billing.index', $data);
    }

    public function LCHistoryPage()
    {
        $data = self::getDataFromDb();
        $data['transactions'] = Transaction::where('users_id', '=', \Auth::user()->id)->get();

        return view('authentication.history', $data);
    }

    public function LCHistoryDetailPage($id)
    {
        $data = self::getDataFromDb();
        $data['transactionDetails'] = TransactionDetails::where('transaction_id', '=', $id)->paginate(5);

        return view('authentication.history-detail', $data);
    }

    public function articlePage()
    {
        $data = self::getDataFromDb();
        $data['article'] = Article::all();

        return view('article.index', $data);
    }

    public function articlePageDetail($slug)
    {
        $data = self::getDataFromDb();
        $data['article_details'] = Article::where('slug', '=', $slug)->first();

        return view('article.blog-detail', $data);
    }

    public function aboutUsPage()
    {
        $data = self::getDataFromDb();
        $data['about_us'] = AboutUsSettings::all();
        return view('aboutus.index', $data);
    }

    public function refundPolicyPage()
    {
        $data = self::getDataFromDb();
        $data['refund_policy'] = RefundPolicySettings::all();
        return view('policy.refund-policy', $data);
    }

    public function termsAndConditionPolicyPage()
    {
        $data = self::getDataFromDb();
        $data['tc_policy'] = TermsConditionPolicySettings::first();
        return view('policy.terms-condition', $data);
    }

    public function photosGuidePage()
    {
        $data = self::getDataFromDb();
        $data['photos_guide'] = GuidePhotosSettings::orderBy('order')->paginate(12);
        return view('photos-guide.index', $data);
    }
}
