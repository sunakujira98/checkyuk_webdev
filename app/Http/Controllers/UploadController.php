<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;
use Session;
use DB;
use Image;
use Auth;

use App\Models\TransactionDetails;
use App\Models\TransactionDetailsImages;

class UploadController extends Controller
{
    //
    public static function uploadImage($transactionId, $codeLC, $name, $image)
    {
        $dt = Carbon::now();
        $month = $dt->format('M');

        $disk = Storage::disk('gcs');
        if (is_null($codeLC) || is_null($name)) {
            $url = 'uploads/' . $month . '/' . $dt->toDateString() . '/' . $transactionId . '_' . Auth::user()->email . '/';
        } else {
            $url = 'uploads/' . $month . '/' . $dt->toDateString() . '/' . $transactionId . '_' . Auth::user()->email . '/' . $codeLC . '_'  . $name . '/';
        }

        $resize_image = Image::make($image->getRealPath());
        $width = $resize_image->height();
        $height = $resize_image->width();

        $image_name = \Util::hyphenize($image->getClientOriginalName());

        if ($width > 1000 || $width > 1000) {
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $resize_image->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->stream();
        } else {
            $resize_image->stream();
        }

        //WATERMARK ONLY FOR PRODUCT PHOTOS
        if (!is_null($codeLC) || !is_null($name)) {
            $watermark = Image::make(public_path('uploads/watermark.png'))->opacity(10);
            $resize_image->insert($watermark, 'center', 10, 10)->stream();
        }

        $disk->put($url . $image_name, $resize_image);

        return $disk->url($url . $image_name);
    }

    public static function uploadMissingImages(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $transactionDetails = TransactionDetails::find((int) $id);
            $transactionId = $transactionDetails->transaction_id;
            $productName = $transactionDetails->product_name;
            $codeLC = $transactionDetails->id_legit_check;
            $transactionDetails->images_is_not_clear = NULL;
            $transactionDetails->save();
            //shan ngoding disini, jadi lu disini upload gambar, tapi foldernya harus sama kaya yang ada di db storage nya sebelumnya, SEMANGATTT
            //MAKASIH

            if ($request->hasFile('image-fotoKurangJelas')) {
                foreach ($request->file('image-fotoKurangJelas') as $files) {
                    $transactionDetailsImages = new TransactionDetailsImages();
                    $transactionDetailsImages->transaction_details_id = (int) $id;
                    $transactionDetailsImages->image_url = self::uploadImage($transactionId, $codeLC, $productName, $files);
                    $transactionDetailsImages->save();
                }
            }

            DB::commit();
            return redirect('lc-history-view/' . $transactionId)->with(['success' => '<strong>Sukses!</strong> Upload gambar berhasil dilakukan! Tunggu proses pengecekan...']);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return redirect()->back()->with(['error' => '<strong>Gagal!</strong> Gagal saat melakukan upload gambar. Silahkan coba kembali dengan gambar yang berbeda.']);
        }
    }
}
