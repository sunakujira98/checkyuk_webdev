<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;
use Session;
use DB;
use Image;
use Auth;

use App\Cart;
use App\Models\Items;
use App\Models\Transaction;
use App\Models\TransactionDetails;
use App\Models\TransactionDetailsImages;
use App\Notifications\EmailUserTransactionToAdmin;

class TransactionController extends Controller
{
    //
    public static function uploadImage($transactionId, $codeLC, $name, $image)
    {
        $dt = Carbon::now();
        $month = $dt->format('M');

        $disk = Storage::disk('gcs');
        if (is_null($codeLC) || is_null($name)) {
            $url = 'uploads/' . $month . '/' . $dt->toDateString() . '/' . $transactionId . '_' . Auth::user()->email . '/';
        } else {
            $url = 'uploads/' . $month . '/' . $dt->toDateString() . '/' . $transactionId . '_' . Auth::user()->email . '/' . $codeLC . '_'  . $name . '/';
        }

        $resize_image = Image::make($image->getRealPath());
        $width = $resize_image->height();
        $height = $resize_image->width();

        $image_name = \Util::hyphenize($image->getClientOriginalName());

        if ($width > 1000 || $width > 1000) {
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $resize_image->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->stream();
        } else {
            $resize_image->stream();
        }

        //WATERMARK ONLY FOR PRODUCT PHOTOS
        if (!is_null($codeLC) || !is_null($name)) {
            $watermark = Image::make(public_path('uploads/watermark.png'))->opacity(10);
            $resize_image->insert($watermark, 'center', 10, 10)->stream();
        }

        $disk->put($url . $image_name, $resize_image);

        return $disk->url($url . $image_name);
    }

    public static function placeOrder(Request $request)
    {
        $cart = Session::get('cart');
        $dt = Carbon::now();

        $lastId = Transaction::max('id') + 1;

        $buktiPembayaran = $request->file('image-buktipembayaran');
        DB::beginTransaction();
        try {
            // table transaction
            $transaction = new Transaction;
            $transaction->users_id = \Auth::user()->id;
            $transaction->total_qty = $cart->totalQty;
            $transaction->grand_total = $request->get('grand_total');
            $transaction->payment_proof_image = self::uploadImage($lastId, null, null, $buktiPembayaran);
            $transaction->save();

            // insert into table detail transaction
            $indexFile = 0;
            $indexBarang = 0;

            foreach ($cart->items as $cartItems) {
                for ($i = 0; $i < $cartItems['qty']; $i++) {
                    $items = Items::select('abbreviation')->where('id', '=', $cartItems['item']['id'])->first();
                    $id = Transaction::max('id');
                    if ($id < 99) {
                        $id = '00' . $id;
                    }
                    $transactionDetails = new TransactionDetails;
                    $transactionDetails->id_legit_check = $items->abbreviation . '-' . $id;
                    $transactionDetails->product_name = $request->get('field-items-' . $indexBarang);
                    $transactionDetails->comment = $request->get('keterangan-' . $indexBarang);
                    $transactionDetails->transaction_id = $transaction->id;
                    $transactionDetails->items_id = $cartItems['item']['id'];
                    $transactionDetails->qty = $cartItems['qty'];
                    $transactionDetails->save();
                    if ($request->hasFile('image-' . $indexFile)) {
                        foreach ($request->file('image-' . $indexFile) as $files) {
                            $transactionDetailsImages = new TransactionDetailsImages();
                            $transactionDetailsImages->transaction_details_id = $transactionDetails->id;
                            $transactionDetailsImages->image_url = self::uploadImage($transaction->id, $items->abbreviation . '-' . $id, $request->get('field-items-' . $indexBarang), $files);
                            $transactionDetailsImages->save();
                        }
                    }
                    $indexBarang++;
                    $indexFile++;
                }
            }
            $transaction->notify(new EmailUserTransactionToAdmin($request->input('field-email'), $transaction->grand_total));
            Session::forget('cart');
            DB::commit();
            return redirect('/lc-history')->with(['success' => '<strong>Sukses!</strong> Pembayaran berhasil dilakukan! Tunggu proses pengecekan...']);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return redirect()->back()->with(['error' => '<strong>Gagal!</strong> Gagal saat melakukan pembayaran, mohon isi kembali form berikut'])->withInput();
        }
    }
}
