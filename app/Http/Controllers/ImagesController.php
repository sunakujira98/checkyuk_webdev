<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemsCategory;

class ImagesController extends Controller
{
    //

    public function post(Request $request)
    {        
        $image_name = $request->image[0]->getClientOriginalName();
        $request->image[0]->move(public_path('upload'), $image_name);
        return response()->json();

    }
}
