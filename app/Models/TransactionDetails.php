<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class TransactionDetails extends Model
{
    //
    use CrudTrait;

    protected $table = 'transaction_details';

    protected $fillable = ['transaction_id', 'product_name', 'is_original', 'items_id', 'qty'];

    function transaction() {
        return $this->belongsTo("App\Models\Transaction", "transaction_id", "id");
    }

    function transaction_details_images() {
        return $this->hasMany("App\Models\TransactionDetailsImages", "transaction_details_id", "id");
    }

    function items() {
        return $this->belongsTo("App\Models\Items", "items_id", "id");
    }
  
}
