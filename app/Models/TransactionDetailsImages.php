<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class TransactionDetailsImages extends Model
{
    //
    use CrudTrait;

    protected $table = 'transaction_details_images';

    protected $fillable = ['transaction_details_id', 'image_url'];

    function transaction_details() {
        return $this->belongsTo("App\Models\TransactionDetails", "transaction_details_id");
    }
}
