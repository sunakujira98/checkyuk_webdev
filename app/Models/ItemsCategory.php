<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ItemsCategory extends Model
{
    //
    use CrudTrait;

    protected $table = 'items_category';

    protected $fillable = ['name', 'image_url','title'];

    function items() {
        return $this->hasMany("App\Models\Items", "items_category_id", "id");
    }
}
