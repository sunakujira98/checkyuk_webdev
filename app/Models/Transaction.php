<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Notifications\Notifiable;

class Transaction extends Model
{
    //
    use CrudTrait;
    use Notifiable;

    protected $table = 'transaction';

    protected $fillable = ['users_id', 'total_qty', 'grand_total', 'payment_proof_image'];

    function transaction_details() {
        return $this->hasMany("App\Models\TransactionDetails", "transaction_id", "id");
    }

    function user() {
        return $this->belongsTo("App\Models\User", "users_id", "id");
    }

    public function routeNotificationForMail($notification)
    {
        return "checkyuk.id@gmail.com";
    }
}
