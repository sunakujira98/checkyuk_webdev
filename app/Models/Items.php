<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Items extends Model
{
    //
    use CrudTrait;

    protected $table = 'items';

    protected $fillable = ['title', 'abbreviation', 'description', 'image_url', 'price', 'items_category_id'];

    function items_category() {
        return $this->belongsTo("App\Models\ItemsCategory", "items_category_id", "id");
    }
}
