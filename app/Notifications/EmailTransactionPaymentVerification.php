<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailTransactionPaymentVerification extends Notification
{
    use Queueable;
    use Notifiable;

    protected $payment_is_verified;
    protected $namaUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($status, $namaUser)
    {
        $this->status = $status;
        $this->namaUser = $namaUser->name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {  
        return (new MailMessage)
            ->subject ('Verifikasi Pembayaran')
            ->markdown('emails.transaction-verify-payment', [
                'payment_is_verified' => $this->status, 
                'namaUser' => $this->namaUser
                ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
