<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailPhotosIsNotClear extends Notification
{
    use Queueable;
    use Notifiable;

    protected $status;
    protected $transactionDetail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($status, $transactionDetail)
    {
        $this->status = $status;
        $this->transactionDetail = $transactionDetail;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject ('Checkyuk - Foto Tidak Jelas Untuk Kode LC ' . $this->transactionDetail->id_legit_check)
            ->markdown('emails.foto-tidakjelas',['status' => $this->status,'transactionDetail' => $this->transactionDetail]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
