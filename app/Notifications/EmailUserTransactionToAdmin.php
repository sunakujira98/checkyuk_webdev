<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailUserTransactionToAdmin extends Notification
{
    use Queueable;
    use Notifiable;

    protected $user;
    protected $totalPrice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $totalPrice)
    {
        $this->user = $user;
        $this->totalPrice = $totalPrice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {  
        return (new MailMessage)
            ->subject ('Transaksi Baru Terjadi')
            ->markdown('emails.userTransaction', [
                'user' => $this->user, 
                'totalPrice' => $this->totalPrice
                ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
