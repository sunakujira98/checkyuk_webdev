<?php

namespace App\Helpers;

class NumberUtil {
    public static function convertNumberToRupiah($angka) {
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
    }
}
